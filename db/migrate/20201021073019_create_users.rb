class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :nickname
      t.string :email
      t.string :avatar
      t.string :phone
      t.string :website
      t.text :intro
      t.boolean :admin, default: false

      t.timestamps
    end
    add_index :users, [:email], unique: true
  end
end
