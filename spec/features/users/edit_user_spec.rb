require 'rails_helper'

RSpec.feature "EditUsers", type: :feature do
  feature "ユーザープロフィール編集画面アクセステスト" do
    given(:user) { create(:user) }
    given(:builder) { create(:user, :builder) }

    context "ユーザーでアクセスした場合" do
      before do
        login user
        visit edit_user_path user
      end

      it "ヘッダーに「プロジェクトの作成」が表示されている" do
        expect(page).to have_content "プロジェクトの作成"
      end

      it "プロフィールの入力フォームが表示される" do
        expect(page).to have_content "プロフィール"
      end
    end

    context "ビルダーでアクセスした場合" do
      before do
        login builder
        visit edit_user_path builder
      end

      it "ヘッダーに「プロジェクトの作成」が表示されない" do
        expect(page).not_to have_content "プロジェクトの作成"
      end

      it "ウェブURLと会社概要の入力フォームが表示される" do
        expect(page).to have_content "ウェブURL"
        expect(page).to have_content "会社概要"
      end
    end
  end

  feature "ユーザープロフィール編集テスト" do
    given(:user) { create(:user) }
    given(:builder) { create(:user, :builder) }
    given(:name) { "Mike" }
    given(:intro) { "これはテストです" }
    given(:weburl) { "http://test.test" }

    context "ユーザーでログインした場合" do
      before do
        login user
        visit edit_user_path user
      end

      context "有効なパラメータの場合" do
        it "ユーザーを編集できる" do
          within(".form") do
            fill_in :user_name, with: name
            fill_in :user_intro, with: intro
            click_on "更新する"
          end
          expect(page).to have_selector '.alert-success', text: 'ユーザーの情報が更新されました。'
          expect(user.reload.name).to eq name
          expect(user.reload.intro).to eq intro
          visit user_path user
          expect(page).to have_content "プロフィール"
          expect(page).to have_content intro
        end
      end

      context "無効なパラメータの場合" do
        it "エラーメッセージが出る" do
          within(".form") do
            fill_in :user_name, with: nil
            click_on "更新する"
          end
          expect(page).to have_selector '.alert-danger', text: "エラーが発生しました。"
          expect(user.reload.name).not_to eq name
        end
      end
    end

    context "ビルダーでログインした場合" do
      before do
        login builder
        visit edit_user_path builder
      end

      it "ウェブURLを編集できる" do
        within(".form") do
          fill_in :user_website, with: weburl
          fill_in :user_intro, with: intro
          click_on "更新する"
        end
        expect(builder.reload.website).to eq weburl
        visit user_path builder
        expect(page).to have_content "会社概要"
        expect(page).to have_content intro
        expect(page).to have_content "会社URL"
        expect(page).to have_content weburl
      end
    end
  end
end
