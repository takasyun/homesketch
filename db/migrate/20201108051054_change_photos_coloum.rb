class ChangePhotosColoum < ActiveRecord::Migration[6.0]
  def change
    remove_column :photos, :photabled_id, :integer
    add_column :photos, :photable_id, :integer
  end
end
