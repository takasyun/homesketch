require 'rails_helper'

RSpec.describe "AccountActivations", type: :request do
  describe "GET edit_account_activation_path" do
    let!(:user) { create(:not_activate_user) }

    context "activation_digestが一致する場合" do
      before do
        get edit_account_activation_url(user.activation_token, email: user.email)
      end

      it "ユーザーページにリダイレクトされる" do
        expect(response).to redirect_to user
      end

      it "認証を促すflashが表示される" do
        expect(flash[:success]).to eq "アカウントの認証が完了いたしました。"
      end

      it "sessionが確立される" do
        expect(session[:user_id]).to eq user.id
      end

      it "cookiesが付与される" do
        expect(cookies[:user_id]).to be_present
        expect(cookies[:remember_token]).to be_present
      end
    end

    context "activation_digestが一致しない場合" do
      before do
        get edit_account_activation_url('invalid', email: user.email)
      end

      it "rootにリダイレクトされる" do
        expect(response).to redirect_to root_url
      end

      it "エラーメッセージが表示される" do
        expect(flash[:danger]).to eq "無効なリンクです。もう一度はじめからやり直してください。"
      end

      it "sessionが確立されない" do
        expect(session[:user_id]).to be_nil
      end

      it "cookiesが付与されない" do
        expect(cookies[:user_id]).to be_nil
        expect(cookies[:remember_token]).to be_nil
      end
    end
  end
end
