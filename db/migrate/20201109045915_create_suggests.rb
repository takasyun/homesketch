class CreateSuggests < ActiveRecord::Migration[6.0]
  def change
    create_table :suggests do |t|
      t.string :title
      t.text :content
      t.string :estimated_amount
      t.references :project, null: false, foreign_key: true

      t.timestamps
    end
  end
end
