class CreateAcceptSuggests < ActiveRecord::Migration[6.0]
  def change
    create_table :accept_suggests do |t|
      t.integer :user_id
      t.integer :approval_suggest_id

      t.timestamps
    end
    add_index :accept_suggests, :user_id
    add_index :accept_suggests, :approval_suggest_id
    add_index :accept_suggests, [:user_id, :approval_suggest_id], unique: true
  end
end
