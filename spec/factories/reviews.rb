FactoryBot.define do
  factory :review do
    content { "test" }
    evaluation { 2 }
    suggest
    customer_id { suggest.project.user.id }
    builder_id { suggest.user.id }
  end
end
