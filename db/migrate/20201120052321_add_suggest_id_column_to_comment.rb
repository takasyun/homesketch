class AddSuggestIdColumnToComment < ActiveRecord::Migration[6.0]
  def change
    add_reference :comments, :suggest, null: false, foreign_key: true
    add_index :comments, [:suggest_id, :user_id], unique: true
  end
end
