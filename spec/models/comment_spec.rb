require 'rails_helper'

RSpec.describe Comment, type: :model do
  shared_examples 'comment_is_invalid?' do
    it "commentは無効である" do
      expect(invalid_comment).to be_invalid
    end
  end

  describe "Commentのバリデーションテスト" do
    context "ユーザー,プロジェクト,コメントがある場合" do
      let!(:valid_comment) { build(:comment) }

      it "commentは有効である" do
        expect(valid_comment).to be_valid
      end
    end

    context "ユーザーが存在しない場合" do
      let(:invalid_comment) { build(:comment, user_id: nil) }

      it_behaves_like 'comment_is_invalid?'
    end

    context "プロジェクトが存在しない場合" do
      let(:invalid_comment) { build(:comment, suggest_id: nil) }

      it_behaves_like 'comment_is_invalid?'
    end

    context "コメントが存在しない場合" do
      let(:invalid_comment) { build(:comment, content: nil) }

      it_behaves_like 'comment_is_invalid?'
    end
  end
end
