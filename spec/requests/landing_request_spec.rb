require 'rails_helper'

RSpec.describe "Landings", type: :request do
  describe "GET /landing" do
    before do
      get root_path
    end

    it "landingページアクセス確認" do
      expect(response).to have_http_status(200)
    end
  end
end
