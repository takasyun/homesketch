require 'rails_helper'

RSpec.describe Project, type: :model do
  describe "Projectバリデーションテスト" do
    context "名前,コンテンツがある場合" do
      let(:valid_project) { build(:project) }

      it "projectは有効である" do
        expect(valid_project).to be_valid
      end
    end

    context "無効なパラメータがある場合" do
      context "名前がない場合" do
        let(:invalid_project) { build(:project, name: nil) }

        it "projectは無効である" do
          expect(invalid_project).to be_invalid
        end
      end

      context "コンテンツがない場合" do
        let(:invalid_project) { build(:project, content: nil) }

        it "projectは無効である" do
          expect(invalid_project).to be_invalid
        end
      end

      context "user_idがない場合" do
        let(:invalid_project) { build(:project, user: nil) }

        it "projectは無効である" do
          expect(invalid_project).to be_invalid
        end
      end

      context "予算が10桁を超える場合" do
        let(:invalid_project) { build(:project, budget: 10000000000) }

        it "projectは無効である" do
          expect(invalid_project).to be_invalid
        end
      end

      context "画像が6枚以上の場合" do
        let(:project) { build(:project) }
        let(:image) {
          Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/sample-salad.jpg')), 'image/png')
        }

        it "projectは無効である" do
          6.times { project.photos.build(image: image) }
          expect(project).to be_invalid
        end
      end
    end
  end
end
