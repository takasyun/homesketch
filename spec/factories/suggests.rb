FactoryBot.define do
  factory :suggest do
    title { "MyString" }
    content { "MyText" }
    estimated_amount { 198 }
    user
    project
  end
end
