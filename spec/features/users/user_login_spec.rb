require 'rails_helper'

RSpec.feature "UserSessions", type: :feature do
  feature "ユーザーログインテスト" do
    given(:user) { create(:user) }
    given(:builder) { create(:user, :builder) }

    before do
      visit login_path
    end

    context "適当なパラメータをフォームに入力した場合" do
      it "ログインに成功して自分のページに移動する" do
        within(".form") do
          fill_in :session_email, with: user.email
          fill_in :session_password, with: user.password
          click_on "ログイン"
        end
        expect(page).to have_title "マイページ"
        expect(page).to have_content user.name
      end
    end

    context "不当なパラメータをフォームに入力した場合" do
      it "エラーメッセージと共にログインページに戻る" do
        within(".form") do
          fill_in :session_email, with: user.email
          fill_in :session_password, with: nil
          click_on "ログイン"
        end
        expect(page).to have_title "ログイン"
        expect(page).to have_selector '.alert-danger', text: 'メールアドレスまたはパスワードが違います'
      end
    end
  end
end
