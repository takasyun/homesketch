class SessionsController < ApplicationController
  def new
    redirect_to current_user if logged_in?
  end

  def create
    @user = User.find_by(email: params[:session][:email])
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        log_in @user
        submit_cookies @user
        redirect_back_or @user
      else
        redirect_to root_url
        flash[:warning] = "送信されたメールからアカウントの有効化を行ってください。"
      end
    else
      flash.now[:danger] = 'メールアドレスまたはパスワードが違います'
      render 'new'
    end
  end

  def destroy
    log_out current_user if logged_in?
    redirect_to root_url
  end
end
