FactoryBot.define do
  factory :user, class: "User" do
    sequence(:name) { |n| "TEST_NAME#{n}" }
    sequence(:email) { |n| "test#{n}@example.com" }
    password { "password" }
    activated { true }
    activated_at { Time.zone.now }

    trait :admin do
      admin { true }
    end

    trait :builder do
      builder { true }
    end

    factory :not_activate_user do
      activated { false }
      activated_at { nil }
    end

    trait :has_project do
      after(:create) do |user|
        user.projects << create(:project)
      end
    end
  end
end
