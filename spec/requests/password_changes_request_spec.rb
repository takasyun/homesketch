require 'rails_helper'

RSpec.describe "PasswordChanges", type: :request do
  shared_examples 'error_and_redirect' do
    it "エラーメッセージと共にリダイレクトされる" do
      expect(flash[:danger]).to eq "先にログインを行ってください。"
      expect(response).to redirect_to login_url
    end
  end

  describe "GET new_password_change_path" do
    let(:user) { create(:user) }

    context "ログインしている場合" do
      before do
        log_in_as user
        get new_password_change_path
      end

      it "パスワード変更ページにアクセスできる" do
        expect(response).to have_http_status(200)
      end
    end

    context "ログインしていない場合" do
      before do
        get new_password_change_path
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "POST new_password_changes_path" do
    let(:user) { create(:user) }

    context "ログインしている場合" do
      before do
        ActionMailer::Base.deliveries.clear
        log_in_as user
        post password_changes_path, params: { id: user.id }
      end

      it "パスワード変更メールが送信される" do
        expect(ActionMailer::Base.deliveries.size).to eq 1
      end

      it "flashが表示される" do
        expect(flash[:info]).to eq "パスワードを変更するためのメールが送信されました。"
      end

      it "ユーザーページにリダイレクトされる" do
        expect(response).to redirect_to user
      end
    end

    context "ログインしていない場合" do
      before do
        ActionMailer::Base.deliveries.clear
        post password_changes_path, params: { id: user.id }
      end

      it "パスワードリセットのメールは送信されない" do
        expect(ActionMailer::Base.deliveries.size).to eq 0
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "GET edit_password_change_path" do
    let(:user) { create(:user) }

    context "ログインしている場合" do
      context "トークン,email共に正しい場合" do
        before do
          log_in_as user
          user.create_change_digest
          get edit_password_change_path(user.change_token, email: user.email)
        end

        it "パスワード変更ページにアクセスできる" do
          expect(response).to have_http_status(200)
        end
      end

      context "チェンジトークンが一致しない場合" do
        before do
          log_in_as user
          user.create_change_digest
          get edit_password_change_path('invalid', email: user.email)
        end

        it "rootにリダイレクトされる" do
          expect(response).to redirect_to root_url
        end

        it "エラーメッセージが表示される" do
          expect(flash[:warning]).to eq 'ユーザーが見つかりません'
        end
      end

      context "該当するユーザーが見つからない場合" do
        before do
          log_in_as user
          user.create_change_digest
          get edit_password_change_path(user.change_token, email: 'invalid@test.com')
        end

        it "rootにリダイレクトされる" do
          expect(response).to redirect_to root_url
        end

        it "エラーメッセージが表示される" do
          expect(flash[:warning]).to eq 'ユーザーが見つかりません'
        end
      end
    end

    context "ログインしていない場合" do
      before do
        user.create_change_digest
        get edit_password_change_path('invalid', email: user.email)
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "PATCH password_change_path" do
    let(:user) { create(:user) }

    context "ログインしている場合" do
      before do
        log_in_as user
        user.create_change_digest
      end

      context "パラメータが全て適当な場合" do
        it "パスワードが変更される" do
          expect do
            patch password_change_path(user.change_token),
                  params: { email: user.email, user: { password: 'validpass', password_confirmation: 'validpass' } }
          end.to change { user.reload.password_digest }
        end
      end

      context "該当するユーザーが見つからない場合" do
        before do
          patch password_change_path(user.change_token),
                params: { email: 'invalid', user: { password: 'validpass', password_confirmation: 'validpass' } }
        end

        it "rootにリダイレクトされる" do
          expect(response).to redirect_to root_url
        end

        it "エラーメッセージが表示される" do
          expect(flash[:warning]).to eq 'ユーザーが見つかりません'
        end
      end

      context "changeトークンが不適当な場合" do
        before do
          patch password_change_path('invalid'),
                params: { email: user.email, user: { password: 'validpass', password_confirmation: 'validpass' } }
        end

        it "rootにリダイレクトされる" do
          expect(response).to redirect_to root_url
        end

        it "エラーメッセージが表示される" do
          expect(flash[:warning]).to eq 'ユーザーが見つかりません'
        end
      end

      context "パスワードが不適当な場合" do
        before do
          patch password_change_path(user.change_token),
                params: { email: user.email, user: { password: nil, password_confirmation: 'validpass' } }
        end

        it "editページに戻される" do
          expect(response.body).to include "パスワードを変更する"
        end
      end
    end

    context "ログインしていない場合" do
      before do
        user.create_change_digest
        patch password_change_path(user.change_token),
              params: { email: user.email, user: { password: "password", password_confirmation: "password" } }
      end

      it_behaves_like 'error_and_redirect'
    end
  end
end
