class ChangeEvaluateBuilderToReview < ActiveRecord::Migration[6.0]
  def change
    rename_table :evaluate_builders, :reviews
  end
end
