require 'rails_helper'

RSpec.describe "Projects", type: :request do
  shared_examples 'error_and_redirect' do
    it "ログインしていないと警告と共にログイン画面にリダイレクトされる" do
      expect(flash[:danger]).to eq "先にログインを行ってください。"
      expect(response).to redirect_to login_url
    end
  end

  describe "GET projects_path" do
    before do
      get projects_path
    end

    it "project投稿ページにアクセスできる" do
      expect(response).to have_http_status(200)
    end
  end

  describe "GET project_path" do
    let(:user) { create(:user) }
    let(:builder) { create(:user, :builder) }

    context "ユーザーでログインしている場合" do
      before do
        log_in_as user
        get new_project_path
      end

      it "project投稿ページにアクセスできる" do
        expect(response).to have_http_status(200)
      end
    end

    context "ビルダーでログインしている場合" do
      before do
        log_in_as builder
        get new_project_path
      end

      it "project投稿ページにアクセスできない" do
        expect(response).to redirect_to builder
        expect(flash[:warning]).to eq "ビルダーはプロジェクトを作成できません。"
      end
    end

    context "ログインしていない場合" do
      before do
        get new_project_path
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "POST project_path" do
    let!(:user) { create(:user) }
    let!(:builder) { create(:user, :builder) }
    let!(:image) { fixture_file_upload('spec/fixtures/sample-salad.jpg', 'image/jpg') }

    6.times do |i|
      let!(:"image#{i}") {
        Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/sample-salad.jpg')), 'image/png')
      }
    end

    context "ユーザーでログインしている場合" do
      before do
        log_in_as user
      end

      context "パラメータが適当な場合" do
        it "projectを写真付きで投稿できる" do
          expect do
            post projects_path, params: { project: {
              name: "testHouse",
              content: "test",
              budget: 100,
              photos_attributes: [image: image],
            } }
          end.to change(Project, :count).by(1).and change(Photo, :count).by(1)
        end

        it "作成したprojectページにリダイレクトされる" do
          post projects_path, params: { project: { name: "testHouse", content: "test", budget: 100 } }
          expect(response).to redirect_to user.projects.first
        end
      end

      context "パラメータが不適当な場合" do
        it "projectは作成されない" do
          expect do
            post projects_path, params: { project: { name: nil, content: "test", budget: 100 } }
          end.to change(Project, :count).by(0)
        end

        it "エラーメッセージが表示される" do
          post projects_path, params: { project: { name: nil, content: "test", budget: 100 } }
          expect(response.body).to include "エラーが発生しました。"
        end
      end

      context "画像枚数が6枚以上の場合" do
        it "projectは投稿できない" do
          expect do
            post projects_path, params: { project: {
              name: "test",
              content: "test",
              budget: 100,
              photos_attributes: {
                "0": { image: image0 },
                "1": { image: image1 },
                "2": { image: image2 },
                "3": { image: image3 },
                "4": { image: image4 },
                "5": { image: image5 },
              },
            } }
          end.to change(Project, :count).by(0).and change(Photo, :count).by(0)
        end
      end
    end

    context "ビルダーでログインしている場合" do
      before do
        log_in_as builder
      end

      it "projectを作成できない" do
        expect do
          post projects_path, params: { project: {
            name: "testHouse",
            content: "test",
            budget: 100,
            photos_attributes: [image: image],
          } }
        end.to change(Project, :count).by(0).and change(Photo, :count).by(0)
      end

      it "マイページにリダイレクトされる" do
        post projects_path, params: { project: {
          name: "testHouse",
          content: "test",
          budget: 100,
          photos_attributes: [image: image],
        } }
        expect(response).to redirect_to builder
        expect(flash[:warning]).to eq "ビルダーはプロジェクトを作成できません。"
      end
    end

    context "ログインしていない場合" do
      before do
        post projects_path, params: { project: { name: "testHouse", content: "test", budget: 100 } }
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "DELETE project_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }
    let!(:project) { create(:project, user: user) }

    context "ログインしている場合" do
      before do
        log_in_as user
      end

      it "projectを削除できる" do
        expect { delete project_path(project) }.to change(Project, :count).by(-1)
      end
    end

    context "別ユーザーでログインしている場合" do
      before do
        log_in_as other_user
      end

      it "projectを削除できない" do
        expect { delete project_path(project) }.to change(Project, :count).by(0)
      end
    end

    context "ログインしていない場合" do
      before do
        delete project_path(project)
      end

      it_behaves_like 'error_and_redirect'
    end
  end
end
