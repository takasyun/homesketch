class RemoveSuggestUserIndexToComment < ActiveRecord::Migration[6.0]
  def change
    remove_index :comments, column: [:suggest_id, :user_id], unique: true
  end
end
