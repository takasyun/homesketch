require 'rails_helper'

RSpec.describe Photo, type: :model do
  describe "photoバリデーションテスト" do
    context "有効なパラメータの場合" do
      let(:comment) { create(:comment) }
      let(:project) { create(:project) }
      let(:image) { fixture_file_upload('spec/fixtures/sample-salad.jpg', 'image/jpg') }
      let(:project_photo) { project.photos.build(image: image) }

      context "プロジェクトに画像をアップロードする場合" do
        it "photoは有効である" do
          expect(project_photo).to be_valid
        end
      end
    end
  end
end
