class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user?, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user ||= User.find_by(email: params[:password_reset][:email])
    if @user.present?
      @user.create_reset_digest
      UserMailer.password_reset(@user).deliver_now
      flash[:info] = "パスワードをリセットするためのリンクが送信されました。"
      redirect_to root_url
    else
      flash.now[:danger] = "メールアドレスが正しくありません。"
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:user][:password].blank?
      @user.errors.add(:password, :blank)
      render 'edit'
    elsif @user.update(user_params)
      log_in @user
      flash[:success] = "パスワードが変更されました"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end

  def valid_user?
    if !(@user && @user.activated? && @user.authenticated?(:reset, params[:id]))
      flash[:warning] = "ユーザーが見つかりません"
      redirect_to root_url
    end
  end

  def check_expiration
    if @user.digest_expired?(:reset)
      flash[:warning] = "リンクの有効期限が切れています。もう一度はじめからやり直してください。"
      redirect_to new_password_reset_url
    end
  end
end
