require 'rails_helper'

RSpec.describe "Relationships", type: :request do
  describe "POST relationships_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }

    context "ログインしている場合" do
      before do
        log_in_as user
      end

      it "フォロー中の人数が増える" do
        expect do
          post relationships_path, params: { followed_id: other_user.id }
        end.to change(user.following, :count).by(1)
      end
    end

    context "ログインしていない場合" do
      it "ログイン画面に飛ばされる" do
        post relationships_path, params: { followed_id: other_user.id }
        expect(response).to redirect_to login_url
      end
    end
  end

  describe "DELETE relationship_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }
    let!(:relationship) { user.active_relationships.create(followed_id: other_user.id) }

    context "ログインしている場合" do
      before do
        log_in_as user
      end

      it "フォローが解除される" do
        expect do
          delete relationship_path relationship
        end.to change(user.following, :count).by(-1)
      end
    end

    context "ログインしていない場合" do
      it "ログイン画面に飛ばされる" do
        delete relationship_path relationship
        expect(response).to redirect_to login_url
      end
    end
  end
end
