require 'rails_helper'

RSpec.describe "AcceptSuggests", type: :request do
  shared_examples 'error_and_redirect' do
    it "エラーメッセージと共にリダイレクトされる" do
      expect(flash[:danger]).to eq "先にログインを行ってください。"
      expect(response).to redirect_to login_url
    end
  end

  describe "POST /accept_suggests_path" do
    let!(:user) { create(:user, :has_project) }
    let!(:suggest) { create(:suggest, project: user.projects.first) }
    let!(:other_user) { create(:user) }

    context "正しいユーザーでログインしている場合" do
      before do
        log_in_as user
      end

      it "提案を受け入れることができる" do
        expect do
          post accept_suggests_path, params: { suggest_id: suggest.id }
        end.to change(user.approve_suggests, :count).by(1)
      end

      it "メッセージと共に提案受け入れ画面にリダイレクトされる" do
        post accept_suggests_path, params: { suggest_id: suggest.id }
        expect(response).to redirect_to accept_suggest_url(suggest)
      end
    end

    context "他ユーザーでログインしている場合" do
      before do
        log_in_as other_user
      end

      it "提案を受け入れることはできない" do
        expect do
          post accept_suggests_path, params: { suggest_id: suggest.id }
        end.to change(user.approve_suggests, :count).by(0).and change(other_user.approve_suggests, :count).by(0)
      end

      it "エラーメッセージと共にリダイレクトされる" do
        post accept_suggests_path, params: { suggest_id: suggest.id }
        expect(flash[:warning]).to eq "プロジェクトの発注者しか提案の受け入れはできません。"
        expect(response).to redirect_to root_url
      end
    end

    context "ログインしていない場合" do
      before do
        post accept_suggests_path, params: { suggest_id: suggest.id }
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "GET /accept_suggest_path" do
    let!(:user) { create(:user, :has_project) }
    let!(:suggest) { create(:suggest, project: user.projects.first) }
    let!(:other_user) { create(:user) }

    context "正しいユーザーでログインしている場合" do
      context "提案を受け入れている場合" do
        before do
          log_in_as user
          user.approve_suggests << suggest
          get accept_suggest_path(suggest)
        end

        it "提案受け入れ画面にアクセスできる" do
          expect(response).to have_http_status(200)
        end
      end

      context "提案を受け入れていない場合" do
        before do
          log_in_as user
          get accept_suggest_path(suggest)
        end

        it "提案受け入れ画面にアクセスできない" do
          expect(flash[:warning]).to eq "提案を受け入れていないユーザーが提案受諾画面にアクセスすることはできません。"
          expect(response).to redirect_to project_suggest_url(suggest.project, suggest)
        end
      end
    end

    context "他ユーザーでログインしている場合" do
      before do
        log_in_as other_user
        get accept_suggest_path(suggest)
      end

      it "エラーメッセージと共にリダイレクトされる" do
        expect(flash[:warning]).to eq "プロジェクトの発注者しか提案の受け入れはできません。"
        expect(response).to redirect_to root_url
      end
    end

    context "ログインしていない場合" do
      before do
        get accept_suggest_path(suggest)
      end

      it_behaves_like 'error_and_redirect'
    end
  end
end
