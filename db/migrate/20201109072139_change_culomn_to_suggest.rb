class ChangeCulomnToSuggest < ActiveRecord::Migration[6.0]
  def change
    change_column :suggests, :estimated_amount, :decimal, precision: 10
  end
end
