class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy, :following, :followers]
  before_action :correct_user, only: [:edit, :update, :destroy]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.account_activation(@user).deliver_now
      flash[:info] = "認証メールを送信いたしました。アカウント認証後にサービスをご利用いただけます。"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
    if @user.builder?
      @projects = Project.where(id: @user.suggests.map(&:project_id)).
        includes(:user).
        paginate(page: params[:page], per_page: 5)
      @star = @user.evaluations
    else
      @projects = @user.projects.paginate(page: params[:page], per_page: 5)
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(edit_user_params)
      flash[:success] = "ユーザーの情報が更新されました。"
      redirect_to edit_user_path(@user)
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    log_out current_user if logged_in?
    flash.now[:success] = "アカウントを削除しました"
    redirect_to root_url
  end

  def following
    @title = "フォロー中"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page], per_page: 10)
    render 'show_follow'
  end

  def followers
    @title = "フォロワー"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page], per_page: 10)
    render 'show_follow'
  end

  private

  def user_params
    params.require(:user).permit(:name,
                                 :email,
                                 :password,
                                 :password_confirmation,
                                 :builder)
  end

  def edit_user_params
    params.require(:user).permit(:name,
                                 :avatar,
                                 :website,
                                 :intro)
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to root_url unless current_user?(@user)
  end
end
