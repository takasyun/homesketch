require 'rails_helper'

RSpec.describe "EmailChanges", type: :request do
  shared_examples 'error_and_redirect' do
    it "エラーメッセージと共にリダイレクトされる" do
      expect(flash[:danger]).to eq "先にログインを行ってください。"
      expect(response).to redirect_to login_url
    end
  end

  describe "GET /edit_email_change_path" do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }

    context "正しいユーザーでログインしている場合" do
      before do
        log_in_as user
        get edit_email_change_path(user)
      end

      it "Eメールアドレス編集画面にアクセスできる" do
        expect(response).to have_http_status(200)
      end
    end

    context "別ユーザーでログインしている場合" do
      before do
        log_in_as other_user
        get edit_email_change_path(user)
      end

      it "エラーメッセージと共にホーム画面にリダイレクトされる" do
        expect(response).to redirect_to root_url
        expect(flash[:danger]).to eq "編集しているユーザーとログインしているアカウントが一致しません。"
      end
    end

    context "ログインしていない場合" do
      before do
        get edit_email_change_path(user)
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "UPDATE /email_change_path" do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }
    let(:new_email) { "new-adress@test.com" }

    context "正しいユーザーでログインしている場合" do
      before do
        log_in_as user
      end

      context "パスワードが正しい場合" do
        it "Eメールアドレスを更新することができる" do
          expect(user.email).not_to eq new_email
          patch email_change_path(user), params: { user: { email: new_email, password: user.password } }
          expect(user.reload.email).to eq new_email
        end
      end

      context "パスワードが正しくない場合" do
        it "Eメールアドレスを更新することができる" do
          expect(user.email).not_to eq new_email
          patch email_change_path(user), params: { user: { email: new_email, password: 'invalid' } }
          expect(user.reload.email).not_to eq new_email
        end
      end
    end

    context "別ユーザーでログインしている場合" do
      before do
        log_in_as other_user
      end

      it "エラーメッセージと共にホーム画面にリダイレクトされる" do
        patch email_change_path(user), params: { user: { email: new_email, password: user.password } }
        expect(response).to redirect_to root_url
        expect(flash[:danger]).to eq "編集しているユーザーとログインしているアカウントが一致しません。"
      end

      it "Eメールアドレスを更新することができない" do
        expect(user.email).not_to eq new_email
        patch email_change_path(user), params: { user: { email: new_email, password: user.password } }
        expect(user.reload.email).not_to eq new_email
      end
    end

    context "ログインしていない場合" do
      before do
        patch email_change_path(user), params: { user: { email: new_email, password: user.password } }
      end

      it_behaves_like 'error_and_redirect'
    end
  end
end
