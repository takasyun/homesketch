class AddUniquenessToSuggests < ActiveRecord::Migration[6.0]
  def change
    remove_index :suggests, column: [:user_id, :project_id]
    add_index :suggests, [:user_id, :project_id], unique: true
  end
end
