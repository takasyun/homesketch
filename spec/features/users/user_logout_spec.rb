require 'rails_helper'

RSpec.feature "UserLogouts", type: :feature do
  feature "ユーザーログアウトテスト" do
    given!(:user) { create(:user) }

    it "ログインした後にログアウトする" do
      visit root_path
      within("header") { click_on "ログイン" }
      within(".form") do
        fill_in :session_email, with: user.email
        fill_in :session_password, with: "password"
        click_on "ログイン"
      end
      within(".dropdown-menu") { click_on "ログアウト" }
      expect(page).to have_content "ログイン"
    end
  end
end
