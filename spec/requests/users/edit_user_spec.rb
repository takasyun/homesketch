require 'rails_helper'

RSpec.describe "EditUser", type: :request do
  describe "GET edit_user_path" do
    let!(:user) { create(:user) }

    context "ログインしている場合" do
      before do
        log_in_as user
        get edit_user_path user
      end

      it "editページにアクセスできる" do
        expect(response).to have_http_status(200)
      end

      it "userを取得できている" do
        expect(response.body).to include user.name
      end
    end

    context "ログインしていない場合" do
      it "ログインページに飛ばされる" do
        get edit_user_path user
        expect(response).to redirect_to login_url
      end
    end
  end

  describe "PATCH user_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }
    let!(:edit_name) { "Mike" }
    let!(:image) { fixture_file_upload('spec/fixtures/sample-salad.jpg', 'image/jpg') }

    context "ログインしている場合" do
      before do
        log_in_as user
      end

      it "ユーザーを編集できる" do
        expect(user.name).not_to eq edit_name
        patch user_path(user), params: { user: { name: edit_name } }
        expect(user.reload.name).to eq edit_name
      end

      it "アバターを編集できる" do
        patch user_path(user), params: { user: { avatar: image } }
        expect(user.reload.avatar).to be_present
      end
    end

    context "ログインしていない場合" do
      it "ログインページに飛ばされる" do
        patch user_path(user), params: { user: { name: edit_name } }
        expect(response).to redirect_to login_url
      end
    end

    context "別のユーザーのプロフィールを編集しようとした場合" do
      before do
        log_in_as user
      end

      it "rootに飛ばされる" do
        patch user_path(other_user), params: { user: { name: edit_name } }
        expect(response).to redirect_to root_url
      end
    end
  end
end
