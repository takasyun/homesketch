require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe "アカウント認証メール" do
    let(:user) { create(:not_activate_user) }
    let(:mail) { UserMailer.account_activation(user) }

    it "メールのヘッダーは正しい情報が記載されている" do
      expect(mail.subject).to eq "HomeSketch アカウント認証メール"
      expect(mail.to).to eq [user.email]
      expect(mail.from).to eq ["noreply@example.com"]
    end

    it "メールの内容がaccount_activationのものである" do
      expect(mail.text_part.body.to_s).to match "#{user.name}さん、HomeSketchへようこそ"
      expect(mail.html_part.body.to_s).to match "#{user.name}さん、HomeSketchへようこそ"
    end

    it "activate_tokenが記載されている" do
      expect(mail.text_part.body.to_s).to match user.activation_token
      expect(mail.html_part.body.to_s).to match user.activation_token
    end
  end

  describe "パスワードリセットメール" do
    let(:user) { create(:user) }
    let(:mail) { UserMailer.password_reset(user) }

    before do
      user.create_reset_digest
    end

    it "メールのヘッダーは正しい情報が記載されている" do
      expect(mail.subject).to eq "HomeSketch パスワードリセット"
      expect(mail.to).to eq [user.email]
      expect(mail.from).to eq ["noreply@example.com"]
    end

    it "メールの内容がパスワードリセットのものである" do
      expect(mail.text_part.body.to_s).to match "HomeSketchのパスワードリセットを受け付けました。"
      expect(mail.html_part.body.to_s).to match "HomeSketchのパスワードリセットを受け付けました。"
    end

    it "reset_tokenが記載されている" do
      expect(mail.text_part.body.to_s).to match user.reset_token
      expect(mail.html_part.body.to_s).to match user.reset_token
    end
  end

  describe "パスワード変更メール" do
    let(:user) { create(:user) }
    let(:mail) { UserMailer.password_change(user) }

    before do
      user.create_change_digest
    end

    it "メールのヘッダーは正しい情報が記載されている" do
      expect(mail.subject).to eq "HomeSketch パスワード変更"
      expect(mail.to).to eq [user.email]
      expect(mail.from).to eq ["noreply@example.com"]
    end

    it "メールの内容がパスワードリセットのものである" do
      expect(mail.text_part.body.to_s).to match "HomeSketchのパスワード変更を受け付けました。"
      expect(mail.html_part.body.to_s).to match "HomeSketchのパスワード変更を受け付けました。"
    end

    it "reset_tokenが記載されている" do
      expect(mail.text_part.body.to_s).to match user.change_token
      expect(mail.html_part.body.to_s).to match user.change_token
    end
  end
end
