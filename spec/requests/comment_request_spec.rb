require 'rails_helper'

RSpec.describe "Comments", type: :request do
  describe "POST comments_path" do
    let!(:user) { create(:user) }
    let!(:suggest) { create(:suggest) }

    context "ログインしている場合" do
      before do
        log_in_as user
      end

      context "パラメータが正常である場合" do
        let(:valid_params) { attributes_for(:comment, suggest_id: suggest.id, user_id: user.id) }

        it "コメントを写真付きで投稿できる" do
          expect do
            post comments_path, params: { comment: valid_params }
          end.to change(Comment, :count).by(1)
        end
      end

      context "コメントの中身がない場合" do
        let(:invalid_params) { attributes_for(:comment, content: nil, suggest_id: suggest.id, user_id: user.id) }

        it "コメントを投稿できない" do
          expect do
            post comments_path, params: { comment: invalid_params }
          end.to change(Comment, :count).by(0)
        end

        it "失敗のフラッシュが表示される" do
          post comments_path, params: { comment: invalid_params }
          expect(flash[:warning]).to eq "コメントの投稿に失敗しました。"
        end
      end

      context "suggestが存在しない場合" do
        let(:invalid_params) { attributes_for(:comment, suggest_id: nil, user_id: user.id) }

        it "コメントを投稿できない" do
          expect do
            post comments_path, params: { comment: invalid_params }
          end.to change(Comment, :count).by(0)
        end

        it "失敗のフラッシュが表示される" do
          post comments_path, params: { comment: invalid_params }
          expect(flash[:warning]).to eq "コメントの投稿に失敗しました。"
        end
      end
    end

    context "ログインしていない場合" do
      let(:valid_params) { attributes_for(:comment, suggest_id: suggest.id, user_id: user.id) }

      it "コメントが投稿できない" do
        expect do
          post comments_path, params: { comment: valid_params }
        end.to change(Comment, :count).by(0)
      end

      it "エラーメッセージと共にログイン画面にリダイレクトされる" do
        post comments_path, params: { comment: valid_params }
        expect(response).to redirect_to login_url
        expect(flash[:danger]).to eq "先にログインを行ってください。"
      end
    end
  end

  describe "DELETE comment_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }
    let!(:comment) { create(:comment, user: user) }

    context "投稿したユーザーでログインした場合" do
      before do
        log_in_as user
      end

      it "コメントを削除できる" do
        expect do
          delete comment_path(comment)
        end.to change(Comment, :count).by(-1)
      end

      it "コメント削除に成功したフラッシュが表示される" do
        delete comment_path(comment)
        expect(flash[:success]).to eq "コメントが削除されました。"
      end
    end

    context "他ユーザーでログインした場合" do
      before do
        log_in_as other_user
      end

      it "コメントを削除できない" do
        expect do
          delete comment_path(comment)
        end.to change(Comment, :count).by(0)
      end

      it "コメントが見つからない旨のフラッシュが表示される" do
        delete comment_path(comment)
        expect(flash[:warning]).to eq "指定されたコメントは見つかりませんでした。"
      end
    end

    context "ログインしていない場合" do
      it "コメントを削除できない" do
        expect do
          delete comment_path(comment)
        end.to change(Comment, :count).by(0)
      end

      it "エラーメッセージと共にログイン画面にリダイレクトされる" do
        delete comment_path(comment)
        expect(response).to redirect_to login_url
        expect(flash[:danger]).to eq "先にログインを行ってください。"
      end
    end
  end
end
