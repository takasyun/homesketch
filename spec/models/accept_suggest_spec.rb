require 'rails_helper'

RSpec.describe AcceptSuggest, type: :model do
  describe "accept_suggestバリデーションテスト" do
    context "user_idとapproval_suggest_idがある場合" do
      let(:valid_accept_suggest) { build(:accept_suggest) }

      it "accept_suggestは有効である" do
        expect(valid_accept_suggest).to be_valid
      end
    end

    context "user_idがない場合" do
      let(:invalid_accept_suggest) { build(:accept_suggest, user: nil) }

      it "accept_suggestは無効である" do
        expect(invalid_accept_suggest).to be_invalid
      end
    end

    context "approval_suggest_idがない場合" do
      let(:invalid_accept_suggest) { build(:accept_suggest, approval_suggest_id: nil) }

      it "accept_suggestは無効である" do
        expect(invalid_accept_suggest).to be_invalid
      end
    end
  end
end
