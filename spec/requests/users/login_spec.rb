require 'rails_helper'

RSpec.describe "Login", type: :request do
  describe "GET,POST /login_path" do
    let(:user) { create(:user) }
    let(:not_activate_user) { create(:not_activate_user) }

    it "ログインページのアクセス確認" do
      get login_path
      expect(response).to have_http_status(200)
    end

    context "パラメータが適当な場合" do
      before do
        post login_path, params: { session: { email: user.email, password: user.password } }
      end

      it "ユーザーページにリダイレクトされる" do
        expect(response).to have_http_status(302)
        expect(response).to redirect_to user
      end

      it "sessionが確立される" do
        expect(session[:user_id]).to eq user.id
      end

      it "cookieが付与される" do
        expect(cookies[:user_id]).to be_present
        expect(cookies[:remember_token]).to be_present
      end
    end

    context "パラメータが不当な場合" do
      before do
        post login_path, params: { session: { email: user.email, password: nil } }
      end

      it "ユーザーページにはリダイレクトされない" do
        expect(response).to have_http_status(200)
        expect(response).not_to redirect_to user
      end

      it "エラーメッセージが表示される" do
        expect(response.body).to include 'メールアドレスまたはパスワードが違います'
      end

      it "sessionは確立されない" do
        expect(session[:user_id]).to be_nil
      end

      it "cookieは付与されない" do
        expect(cookies[:user_id]).to be_nil
        expect(cookies[:remember_token]).to be_nil
      end
    end

    context "ユーザー認証が行われていない場合" do
      before do
        post login_path, params: { session: { email: not_activate_user.email, password: not_activate_user.password } }
      end

      it "rootページにリダイレクトされる" do
        expect(response).to redirect_to root_url
      end

      it "エラーメッセージが表示される" do
        expect(flash[:warning]).to eq '送信されたメールからアカウントの有効化を行ってください。'
      end
    end
  end
end
