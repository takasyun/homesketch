FactoryBot.define do
  factory :project, class: 'Project' do
    sequence(:name) { |n| "house-name#{n}" }
    sequence(:content) { |n| "test_text#{n}" }
    budget { 100 }
    user
  end
end
