class AcceptSuggest < ApplicationRecord
  belongs_to :user
  belongs_to :approval_suggest, class_name: 'Suggest'

  validates :user_id, presence: true
  validates :approval_suggest_id, presence: true
end
