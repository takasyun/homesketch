FactoryBot.define do
  factory :comment, class: "Comment" do
    content { "test" }
    user
    suggest
  end
end
