class Project < ApplicationRecord
  belongs_to :user
  has_many :photos, as: :photable, inverse_of: :photable, dependent: :destroy
  has_many :suggests, dependent: :destroy

  accepts_nested_attributes_for :photos

  validates :name, presence: true
  validates :user_id, presence: true
  validates :content, presence: true
  validates :budget, numericality: { only_integer: true }, length: { maximum: 10 }, presence: true
  validates :photos, length: { maximum: 5 }
end
