class CreatePhotos < ActiveRecord::Migration[6.0]
  def change
    create_table :photos do |t|
      t.string :image
      t.integer :photabled_id
      t.string :photable_type

      t.timestamps
    end
    add_index :photos, [:photabled_id, :photable_type]
  end
end
