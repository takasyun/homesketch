class Comment < ApplicationRecord
  belongs_to :suggest
  belongs_to :user

  validates :content, presence: true
  validates :user_id, presence: true
  validates :suggest_id, presence: true
end
