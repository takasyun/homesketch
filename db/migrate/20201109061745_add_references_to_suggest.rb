class AddReferencesToSuggest < ActiveRecord::Migration[6.0]
  def change
    add_reference :suggests, :user, null: false, foreign_key: true
    add_index :suggests, [:user_id, :project_id]
  end
end
