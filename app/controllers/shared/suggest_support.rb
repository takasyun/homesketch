module Shared::SuggestSupport
  def builder_already_suggest?(project)
    project.suggests.include? already_suggest(project)
  end

  def already_suggest(project)
    current_user.suggests.find_by(project_id: project.id)
  end
end
