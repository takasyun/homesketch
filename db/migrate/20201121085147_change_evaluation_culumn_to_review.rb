class ChangeEvaluationCulumnToReview < ActiveRecord::Migration[6.0]
  def change
    remove_column :reviews, :evaluation, :integer
    add_column :reviews, :evaluation, :decimal
  end
end
