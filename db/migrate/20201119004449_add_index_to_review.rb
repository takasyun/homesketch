class AddIndexToReview < ActiveRecord::Migration[6.0]
  def change
    add_index :reviews, :customer_id
    add_index :reviews, :builder_id
    add_index :reviews, :suggest_id
    add_index :reviews, [:customer_id, :suggest_id, :builder_id], unique: true
  end
end
