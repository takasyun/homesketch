class SuggestsController < ApplicationController
  include Shared::SuggestSupport

  before_action :logged_in_user
  before_action :correct_user, only: [:edit, :update, :destroy]
  before_action :builder_can_suggest, only: [:new, :create]
  before_action :builder_cant_suggest_twise, only: [:new, :create]

  def new
    redirect_to root_url and return if @project.nil?
    @suggest = current_user.suggests.build
    @suggest.photos.build
  end

  def create
    @suggest = Suggest.new(suggest_params)
    if @suggest.save
      redirect_to project_suggest_url(@suggest.project.id, @suggest.id)
    else
      flash.now[:warning] = "提案の投稿に失敗しました。"
      render 'new'
    end
  end

  def show
    @suggest = Suggest.find_by(id: params[:id])
    redirect_to root_url and return if @suggest.nil?
    @project = @suggest.project
    @customer = @project.user
    @comment = Comment.new
    @comments = @suggest.comments.paginate(page: params[:page], per_page: 10)
  end

  def edit
    @suggest = Suggest.find_by(id: params[:id])
    @suggest.photos.build if @suggest.photos.blank?
  end

  def update
    @suggest = Suggest.find(params[:id])
    if @suggest.update(update_params)
      redirect_to project_suggest_path(@suggest.project, @suggest)
    else
      flash.now[:warning] = "提案の編集に失敗しました。"
      render 'edit'
    end
  end

  def destroy
    @suggest.destroy
    flash[:success] = "提案が削除されました。"
    redirect_to project_url(params[:project_id])
  end

  private

  def suggest_params
    params.require(:suggest).permit(:title,
                                    :content,
                                    :estimated_amount,
                                    :project_id,
                                    photos_attributes: [:image]).merge(user_id: current_user.id)
  end

  def update_params
    params.require(:suggest).permit(:content,
                                    :estimated_amount,
                                    photos_attributes: [:image, :id, :_destroy])
  end

  def correct_user
    @suggest = current_user.suggests.find_by(id: params[:id])
    if @suggest.nil?
      flash.now[:warning] = "指定されたプロジェクトは見つかりませんでした。"
      redirect_to root_url
    end
  end

  def builder_can_suggest
    unless current_user.builder?
      redirect_to current_user
      flash.now[:warning] = "提案を作成できるのはビルダーだけです。"
    end
  end

  def builder_cant_suggest_twise
    @project = Project.find_by(id: params[:project_id]) or Project.find_by(id: params[:suggest][:project_id])
    if builder_already_suggest?(@project)
      redirect_to @project || root_url
      flash[:warning] = "同じプロジェクトへの提案はできません。"
    end
  end
end
