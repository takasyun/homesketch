require 'rails_helper'

RSpec.describe "SendPasswordResetMails", type: :request do
  describe "GET /password_resets_path" do
    before do
      get new_password_reset_path
    end

    it "パスワードリセットにアクセスできる" do
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /password_resets_path" do
    let(:user) { create(:user) }

    context "メールアドレスが存在する場合" do
      before do
        ActionMailer::Base.deliveries.clear
        post password_resets_path, params: { password_reset: { email: user.email } }
      end

      it "rootにリダイレクトされる" do
        expect(response).to redirect_to root_url
      end

      it "flashが表示される" do
        expect(flash[:info]).to eq "パスワードをリセットするためのリンクが送信されました。"
      end

      it "パスワードリセットのメールが送信される" do
        expect(ActionMailer::Base.deliveries.size).to eq 1
      end
    end

    context "メールアドレスが存在しない場合" do
      before do
        ActionMailer::Base.deliveries.clear
        post password_resets_path, params: { password_reset: { email: "invalid" } }
      end

      it "エラーが表示される" do
        expect(flash[:danger]).to eq "メールアドレスが正しくありません。"
      end

      it "メール入力画面に留まる" do
        expect(response).to have_http_status(200)
        expect(response.body).to include "パスワードをリセットする"
      end

      it "パスワードリセットのメールは送信されない" do
        expect(ActionMailer::Base.deliveries.size).to eq 0
      end
    end
  end
end
