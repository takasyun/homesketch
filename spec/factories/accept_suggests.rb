FactoryBot.define do
  factory :accept_suggest, class: "AcceptSuggest" do
    user
    approval_suggest_id { association(:suggest)[:id] }
  end
end
