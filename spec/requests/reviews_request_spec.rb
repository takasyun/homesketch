require 'rails_helper'

RSpec.describe "Reviews", type: :request do
  shared_examples 'error_and_redirect' do
    it "エラーメッセージと共にリダイレクトされる" do
      expect(flash[:danger]).to eq "先にログインを行ってください。"
      expect(response).to redirect_to login_url
    end
  end

  describe "GET /reviews_path" do
    let(:user) { create(:user) }
    let(:review) { create(:review) }

    context "ログインしている場合" do
      context "paramsがある場合" do
        before do
          log_in_as user
          get reviews_path, params: { id: review.suggest.user.id }
        end

        it "review-indexにアクセスできる" do
          expect(response).to have_http_status(200)
        end
      end

      context "paramsがない場合" do
        before do
          log_in_as user
          get reviews_path
        end

        it "review-indexにアクセスできない" do
          expect(response).to redirect_to root_url
        end
      end
    end

    context "ログインしていない場合" do
      before do
        get reviews_path, params: { id: review.suggest.user.id }
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "GET /new_review_path" do
    let(:user) { create(:user, :has_project) }
    let(:other_user) { create(:user) }
    let(:builder) { create(:user, :builder) }
    let(:suggest) { create(:suggest, user: builder, project: user.projects.first) }

    context "プロジェクトの発注者でログインしている場合" do
      context "まだレビューを投稿してないかつ提案を受け入れている場合" do
        before do
          log_in_as user
          user.approve_suggests << suggest
          get new_review_path, params: { review: { suggest_id: suggest.id } }
        end

        it "レビュー投稿画面にアクセスできる" do
          expect(response).to have_http_status(200)
        end
      end

      context "すでにレビューを投稿している場合" do
        let!(:review) { create(:review, suggest: suggest) }

        before do
          log_in_as user
          get new_review_path, params: { review: { suggest_id: suggest.id } }
        end

        it "エラーメッセージと共にマイページにリダイレクトされる" do
          expect(response).to redirect_to user
          expect(flash[:warning]).to eq "すでにレビューを投稿しています。"
        end
      end

      context "該当する提案が存在していなかった場合" do
        before do
          log_in_as user
          get new_review_path, params: { review: { suggest_id: nil } }
        end

        it "エラーメッセージと共にマイページにリダイレクトされる" do
          expect(response).to redirect_to user
          expect(flash[:warning]).to eq "レビューする提案が見つかりませんでした。"
        end
      end

      context "提案を受け入れていない場合" do
        before do
          log_in_as user
          get new_review_path, params: { review: { suggest_id: suggest.id } }
        end

        it "エラーメッセージと共にプロジェクトページにリダイレクトされる" do
          expect(response).to redirect_to suggest.project
          expect(flash[:warning]).to eq "レビューは提案を受け入れたユーザーしかできません。"
        end
      end
    end

    context "関係ないユーザーでログインしている場合" do
      before do
        log_in_as other_user
        get new_review_path, params: { review: { suggest_id: suggest.id } }
      end

      it "エラーメッセージと共にプロジェクト画面にリダイレクトされる" do
        expect(response).to redirect_to suggest.project
        expect(flash[:warning]).to eq "プロジェクト発注者のみがレビューを投稿できます。"
      end
    end

    context "ログインしていない場合" do
      before do
        get new_review_path, params: { review: { suggest_id: suggest.id } }
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "POST /reviews_path" do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }
    let(:builder) { create(:user, :builder) }
    let(:project) { create(:project, user: user) }
    let(:suggest) { create(:suggest, user: builder, project: project) }

    context "プロジェクトの発注者でログインしている場合" do
      context "まだレビューを投稿してないかつ提案を受け入れている場合" do
        before do
          log_in_as user
          user.approve_suggests << suggest
        end

        context "パラメータが正常な場合" do
          let(:valid_review_params) { attributes_for(:review, suggest: suggest, suggest_id: suggest.id) }

          it "レビューを投稿できる" do
            expect do
              post reviews_path, params: { review: valid_review_params }
            end.to change(Review, :count).by(1)
          end
        end

        context "コンテンツがない場合" do
          let(:invalid_review_params) {
            attributes_for(:review,
                           suggest: suggest,
                           suggest_id: suggest.id,
                           content: nil)
          }

          it "レビューを投稿できない" do
            expect do
              post reviews_path, params: { review: invalid_review_params }
            end.to change(Review, :count).by(0)
          end
        end

        context "評価がない場合" do
          let(:invalid_review_params) {
            attributes_for(:review,
                           suggest: suggest,
                           suggest_id: suggest.id,
                           evaluation: nil)
          }

          it "レビューを投稿できない" do
            expect do
              post reviews_path, params: { review: invalid_review_params }
            end.to change(Review, :count).by(0)
          end
        end

        context "評価がない場合" do
          let(:invalid_review_params) {
            attributes_for(:review,
                           suggest: suggest,
                           suggest_id: suggest.id,
                           evaluation: nil)
          }

          it "レビューを投稿できない" do
            expect do
              post reviews_path, params: { review: invalid_review_params }
            end.to change(Review, :count).by(0)
          end
        end
      end

      context "すでにレビューを投稿している場合" do
        let!(:review) { create(:review, suggest: suggest) }
        let(:valid_review_params) { attributes_for(:review, suggest: suggest, suggest_id: suggest.id) }

        before do
          log_in_as user
          user.approve_suggests << suggest
          post reviews_path, params: { review: valid_review_params }
        end

        it "エラーメッセージと共にマイページにリダイレクトされる" do
          post reviews_path, params: { review: valid_review_params }
          expect(response).to redirect_to user
          expect(flash[:warning]).to eq "すでにレビューを投稿しています。"
        end
      end

      context "該当する提案が存在しなかった場合" do
        let(:invalid_review_params) { attributes_for(:review, suggest: suggest, suggest_id: nil) }

        before do
          log_in_as user
          post reviews_path, params: { review: invalid_review_params }
        end

        it "エラーメッセージと共にマイページにリダイレクトされる" do
          expect(response).to redirect_to user
          expect(flash[:warning]).to eq "レビューする提案が見つかりませんでした。"
        end
      end

      context "提案を受け入れていない場合" do
        let(:valid_review_params) { attributes_for(:review, suggest: suggest, suggest_id: suggest.id) }

        before do
          log_in_as user
          post reviews_path, params: { review: valid_review_params }
        end

        it "エラーメッセージと共にプロジェクトページにリダイレクトされる" do
          expect(response).to redirect_to suggest.project
          expect(flash[:warning]).to eq "レビューは提案を受け入れたユーザーしかできません。"
        end
      end
    end

    context "関係ないユーザーでログインしている場合" do
      let(:valid_review_params) { attributes_for(:review, suggest: suggest, suggest_id: suggest.id) }

      before do
        log_in_as other_user
        post reviews_path, params: { review: valid_review_params }
      end

      it "エラーメッセージと共にプロジェクト画面にリダイレクトされる" do
        expect(response).to redirect_to suggest.project
        expect(flash[:warning]).to eq "プロジェクト発注者のみがレビューを投稿できます。"
      end
    end

    context "ログインしていない場合" do
      let(:valid_review_params) { attributes_for(:review, suggest: suggest, suggest_id: suggest.id) }

      before do
        post reviews_path, params: { review: valid_review_params }
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "GET /review_path" do
    let(:user) { create(:user) }
    let(:review) { create(:review) }

    context "ログインしている場合" do
      before do
        log_in_as user
        get review_path review
      end

      it "レビュー画面にアクセスできる" do
        expect(response).to have_http_status(200)
      end
    end

    context "ログインしていない場合" do
      before do
        get review_path review
      end

      it_behaves_like 'error_and_redirect'
    end
  end
end
