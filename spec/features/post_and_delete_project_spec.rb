require 'rails_helper'

RSpec.feature "PostAndDeleteProjects", type: :feature do
  feature "プロジェクト作成テスト" do
    given(:user) { create(:user) }
    given(:project_name) { "Testhouse" }
    given(:project_content) { "test" }
    given(:project_budget) { 100 }
    given(:image_path) { Rails.root.join('spec/fixtures/sample-salad.jpg') }

    before do
      login user
      visit new_project_path
    end

    it "プロジェクトを作成した後にプロジェクトを削除する" do
      expect do
        attach_file('project_photos_attributes_0_image', image_path)
        fill_in :project_name, with: project_name
        fill_in :project_content, with: project_content
        fill_in :project_budget, with: project_budget
        click_on "プロジェクトを投稿する"
      end.to change(Project, :count).by(1).and change(Photo, :count).by(1)
      expect(page).to have_content project_name
      expect(page).to have_content project_content
      expect(page).to have_content project_budget
      expect { click_on "プロジェクトを削除する" }.to change(Project, :count).by(-1).and change(Photo, :count).by(-1)
      expect(page).to have_content "プロジェクトが削除されました。"
    end
  end
end
