require 'rails_helper'

RSpec.feature "PostSuggests", type: :feature do
  feature "提案の作成と削除テスト" do
    given(:builder) { create(:user, :builder) }
    given(:project) { create(:project) }
    given(:title) { "test" }
    given(:content) { "test_content" }
    given(:estimated_amount) { 999 }
    given(:change_estimated_amount) { 200 }
    given(:image_path) { Rails.root.join('spec/fixtures/sample-salad.jpg') }

    before do
      login builder
      visit project_path(project)
    end

    it "提案を投稿した後、提案を編集して削除する" do
      click_on "このプロジェクトに提案する"
      expect do
        attach_file('suggest_photos_attributes_0_image', image_path)
        attach_file('suggest_photos_attributes_1_image', image_path)
        fill_in :suggest_title, with: title
        fill_in :suggest_content, with: content
        fill_in :suggest_estimated_amount, with: estimated_amount
        click_on "提案する"
      end.to change(Suggest, :count).by(1).and change(Photo, :count).by(2)
      expect(page).to have_content builder.name
      expect(page).to have_content title
      expect(page).to have_content content
      expect(page).to have_content estimated_amount
      expect(page).to have_selector '.show-photo-box', count: 2
      click_on "提案を編集する"
      expect do
        attach_file('suggest_photos_attributes_2_image', image_path)
        fill_in :suggest_estimated_amount, with: change_estimated_amount
        click_on "提案を編集する"
      end.to change(Photo, :count).by(1)
      expect(page).to have_content change_estimated_amount
      expect(page).to have_selector '.show-photo-box', count: 3
      expect { click_on "提案を取り下げる" }.to change(Suggest, :count).by(-1).and change(Photo, :count).by(-3)
      expect(page).to have_content "提案が削除されました。"
    end
  end
end
