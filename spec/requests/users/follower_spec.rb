require 'rails_helper'

RSpec.describe "Follower-FollowingView", type: :request do
  describe "GET following_user_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }
    let!(:relationship) { user.active_relationships.create(followed_id: other_user.id) }

    context "ログインしている場合" do
      before do
        log_in_as user
        get following_user_path user
      end

      it "フォロー中一覧のページにアクセスできる" do
        expect(response).to have_http_status(200)
      end

      it "フォロー中のユーザーが表示される" do
        expect(response.body).to include other_user.name
      end
    end

    context "ログインしていない場合" do
      it "ログイン画面にリダイレクトされる" do
        get following_user_path user
        expect(response).to redirect_to login_url
      end
    end
  end

  describe "GET follower_user_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }
    let!(:relationship) { other_user.active_relationships.create(followed_id: user.id) }

    context "ログインしている場合" do
      before do
        log_in_as user
        get followers_user_path user
      end

      it "フォロワー一覧のページにアクセスできる" do
        expect(response).to have_http_status(200)
      end

      it "フォロワーが表示される" do
        expect(response.body).to include other_user.name
      end
    end

    context "ログインしていない場合" do
      it "ログイン画面にリダイレクトされる" do
        get followers_user_path user
        expect(response).to redirect_to login_url
      end
    end
  end
end
