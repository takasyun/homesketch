module Login
  def log_in_as(user)
    post login_path, params: { session: { email: user.email, password: user.password } }
  end

  def login(user)
    visit login_path
    fill_in :session_email, with: user.email
    fill_in :session_password, with: 'password'
    click_button 'ログイン'
  end
end
