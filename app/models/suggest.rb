class Suggest < ApplicationRecord
  PHOTOS_MAXIMUM = 5

  belongs_to :user
  belongs_to :project

  has_many :photos, as: :photable, inverse_of: :photable, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_one :accept_suggest, foreign_key: 'approval_suggest_id', dependent: :destroy
  has_one :recommending_user, through: :accept_suggest, source: :user
  has_one :review, foreign_key: 'suggest_id'

  accepts_nested_attributes_for :photos, allow_destroy: true

  validates :user_id, presence: true
  validates :project_id, presence: true
  validates :title, length: { maximum: 50 }, presence: true
  validates :content, presence: true
  validates :estimated_amount, numericality: { only_integer: true }, length: { maximum: 10 }, presence: true
  validates :photos, length: { maximum: 5 }
end
