require 'rails_helper'

RSpec.describe "PasswordResets", type: :request do
  shared_examples 'error_and_redirect' do
    it "エラーメッセージと共にリダイレクトされる" do
      expect(flash[:warning]).to eq message
      expect(response).to redirect
    end
  end

  describe "GET edit_password_reset_path" do
    let(:user) { create(:user) }
    let(:not_activate_user) { create(:not_activate_user) }

    context "paramsが有効かつ、ユーザーがアクティブな場合" do
      before do
        user.create_reset_digest
        get edit_password_reset_path(user.reset_token, email: user.email)
      end

      it "パスワード再設定の画面にアクセスできる" do
        expect(response).to have_http_status(200)
      end
    end

    context "メールアドレスが不適当な場合" do
      let(:redirect) { redirect_to root_url }
      let(:message) { "ユーザーが見つかりません" }

      before do
        user.create_reset_digest
        get edit_password_reset_path(user.reset_token, email: "invalid")
      end

      it_behaves_like 'error_and_redirect'
    end

    context "ユーザーがアクティブでない場合" do
      let(:redirect) { redirect_to root_url }
      let(:message) { "ユーザーが見つかりません" }

      before do
        not_activate_user.create_reset_digest
        get edit_password_reset_path(not_activate_user.reset_token, email: not_activate_user.email)
      end

      it_behaves_like 'error_and_redirect'
    end

    context "reset_tokenが不適当な場合" do
      let(:redirect) { redirect_to root_url }
      let(:message) { "ユーザーが見つかりません" }

      before do
        user.create_reset_digest
        get edit_password_reset_path("invalid", email: user.email)
      end

      it_behaves_like 'error_and_redirect'
    end

    context "リンクの有効期限が切れている場合" do
      let(:redirect) { redirect_to new_password_reset_url }
      let(:message) { "リンクの有効期限が切れています。もう一度はじめからやり直してください。" }

      before do
        user.create_reset_digest
        user.update_attribute(:reset_sent_at, Time.current.prev_month)
        get edit_password_reset_path(user.reset_token, email: user.email)
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "PATCH password_reset_path" do
    let!(:user) { create(:user) }
    let!(:not_activate_user) { create(:not_activate_user) }
    let!(:new_pass) { "new_pass" }

    context "パスワードが有効な場合" do
      before do
        user.create_reset_digest
      end

      it "パスワードの再設定ができる" do
        expect do
          patch password_reset_path(user.reset_token),
                params: { email: user.email, user: { password: new_pass, password_confirmation: new_pass } }
        end.to change { user.reload.password_digest }
      end

      it "ユーザーページにリダイレクトされる" do
        patch password_reset_path(user.reset_token),
              params: { email: user.email, user: { password: new_pass, password_confirmation: new_pass } }
        expect(response).to redirect_to user
      end

      it "パスワードが変更されたメッセージが表示される" do
        patch password_reset_path(user.reset_token),
              params: { email: user.email, user: { password: new_pass, password_confirmation: new_pass } }
        expect(flash[:success]).to eq "パスワードが変更されました"
      end
    end

    context "ユーザーがアクティブでない場合" do
      let(:redirect) { redirect_to root_url }
      let(:message) { "ユーザーが見つかりません" }

      before do
        not_activate_user.create_reset_digest
        patch password_reset_path(not_activate_user.reset_token),
              params: { email: not_activate_user.email, user: { password: new_pass, password_confirmation: new_pass } }
      end

      it_behaves_like 'error_and_redirect'
    end

    context "メールアドレスが不適当な場合" do
      let(:redirect) { redirect_to root_url }
      let(:message) { "ユーザーが見つかりません" }

      before do
        user.create_reset_digest
        patch password_reset_path(user.reset_token),
              params: { email: "invalid", user: { password: new_pass, password_confirmation: new_pass } }
      end

      it_behaves_like 'error_and_redirect'
    end

    context "reset_tokenが不適当な場合" do
      let(:redirect) { redirect_to root_url }
      let(:message) { "ユーザーが見つかりません" }

      before do
        user.create_reset_digest
        patch password_reset_path("invalid"),
              params: { email: user.email, user: { password: new_pass, password_confirmation: new_pass } }
      end

      it_behaves_like 'error_and_redirect'
    end

    context "パスワードが空の場合" do
      before do
        user.create_reset_digest
        patch password_reset_path(user.reset_token),
              params: { email: user.email, user: { password: nil, password_confirmation: new_pass } }
      end

      it "editページに戻される" do
        expect(response.body).to include "パスワードをリセットする"
      end
    end
  end
end
