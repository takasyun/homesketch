module Shared::SessionSupport
  def log_in(user)
    session[:user_id] = user.id
    submit_cookies user
  end

  def logged_in_user
    unless logged_in?
      store_access_url
      flash[:danger] = "先にログインを行ってください。"
      redirect_to login_url
    end
  end

  def log_out(user)
    session.delete(:user_id)
    delete_cookies user
    @current_user = nil
  end

  def submit_cookies(user)
    user.create_remember_token
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  def delete_cookies(user)
    user.delete_remember_token
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def redirect_back_or(user)
    redirect_to session[:access_url] || user
    session.delete(:access_url)
  end

  def store_access_url
    session[:access_url] = request.original_url if request.get?
  end
end
