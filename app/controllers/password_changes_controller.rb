class PasswordChangesController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update]
  before_action :get_user, only: [:edit, :update]
  before_action :valid_user?, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user = User.find_by(id: params[:id])
    if @user.present?
      @user.create_change_digest
      UserMailer.password_change(@user).deliver_now
      flash[:info] = "パスワードを変更するためのメールが送信されました。"
      redirect_to @user || root_url
    end
  end

  def edit
  end

  def update
    if @user.update(user_params)
      flash.now[:success] = "パスワードが変更されました"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end

  def valid_user?
    if !(@user && @user.authenticated?(:change, params[:id]))
      flash[:warning] = "ユーザーが見つかりません"
      redirect_to root_url
    end
  end

  def check_expiration
    if @user.digest_expired?(:change)
      flash[:warning] = "リンクの有効期限が切れています。もう一度はじめからやり直してください。"
      redirect_to new_password_change_url
    end
  end
end
