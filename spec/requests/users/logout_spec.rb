require 'rails_helper'

RSpec.describe "Logout", type: :request do
  describe "GET /logout" do
    let!(:user) { create(:user) }

    before do
      log_in_as user
      get logout_path
    end

    it "ログアウトするとrootに戻る" do
      expect(response).to have_http_status(302)
      expect(response).to redirect_to root_url
    end

    it "ログアウトするとsessionが切れる" do
      expect(session[:user_id]).to be_nil
    end

    it "ログアウトするとcookiesが消される" do
      expect(cookies[:user_id]).to be_blank
      expect(cookies[:remember_token]).to be_blank
    end
  end
end
