require 'rails_helper'

RSpec.describe "SignupPath", type: :request do
  describe "GET /signup_path" do
    it "Signupページアクセス確認" do
      get signup_path
      expect(response).to have_http_status(200)
    end
  end

  describe "POST /signup_path" do
    context "入力されたパラメータが適当な場合" do
      let(:valid_params) { attributes_for(:not_activate_user) }
      let(:user) { User.find_by(email: valid_params[:email]) }

      before do
        ActionMailer::Base.deliveries.clear
        post signup_path, params: { user: valid_params }
      end

      it "ユーザー登録に成功してrootにリダイレクトされる" do
        expect(response).to have_http_status(302)
        expect(response.body).to redirect_to root_url
      end

      it "アカウント認証メールが送信される" do
        expect(ActionMailer::Base.deliveries.size).to eq 1
      end

      it "アカウント認証を促すメッセージが表示される" do
        expect(flash[:info]).to eq "認証メールを送信いたしました。アカウント認証後にサービスをご利用いただけます。"
      end
    end

    context "パスワードが不適当の場合" do
      let(:invalid_params) { attributes_for(:not_activate_user, password: nil) }

      before do
        ActionMailer::Base.deliveries.clear
        post signup_path, params: { user: invalid_params }
      end

      it "ユーザー登録に失敗してマイページにリダイレクトされない" do
        expect(response).to have_http_status(200)
      end

      it "エラーメッセージが発生する" do
        expect(response.body).to include 'エラーが発生しました'
      end

      it "アカウント認証メールは送信されない" do
        expect(ActionMailer::Base.deliveries.size).to eq 0
      end
    end

    context "パスワードと確認用パスワードが違う場合" do
      let(:invalid_params) { attributes_for(:not_activate_user, password: "password", password_confirmation: "foobar") }

      before do
        post signup_path, params: { user: invalid_params }
      end

      it "ユーザー登録に失敗してマイページにリダイレクトされない" do
        expect(response).to have_http_status(200)
      end

      it "エラーメッセージが発生する" do
        expect(response.body).to include 'エラーが発生しました'
      end
    end
  end
end
