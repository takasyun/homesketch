class AddProjectIdToReview < ActiveRecord::Migration[6.0]
  def change
    add_column :reviews, :suggest_id, :string
  end
end
