require 'rails_helper'

RSpec.describe Suggest, type: :model do
  describe "Suggestのバリデーションテスト" do
    context "バリデーションを満たしている場合" do
      let(:valid_suggest) { build(:suggest) }

      it "suggestは有効である" do
        expect(valid_suggest).to be_valid
      end
    end

    context "無効なパラメータがある場合" do
      context "タイトルがない場合" do
        let(:invalid_suggest) { build(:suggest, title: nil) }

        it "suggestは無効である" do
          expect(invalid_suggest).to be_invalid
        end
      end

      context "タイトルが50文字以上の場合" do
        let(:invalid_suggest) { build(:suggest, title: "a" * 51) }

        it "suggestは無効である" do
          expect(invalid_suggest).to be_invalid
        end
      end

      context "提案の内容がない場合" do
        let(:invalid_suggest) { build(:suggest, content: nil) }

        it "suggestは無効である" do
          expect(invalid_suggest).to be_invalid
        end
      end

      context "見積もり金額がない場合" do
        let(:invalid_suggest) { build(:suggest, estimated_amount: nil) }

        it "suggestは無効である" do
          expect(invalid_suggest).to be_invalid
        end
      end

      context "見積もり金額が10桁を超える場合" do
        let(:invalid_suggest) { build(:suggest, estimated_amount: 10000000000) }

        it "suggestは無効である" do
          expect(invalid_suggest).to be_invalid
        end
      end

      context "ユーザーidがない場合" do
        let(:invalid_suggest) { build(:suggest, user: nil) }

        it "suggestは無効である" do
          expect(invalid_suggest).to be_invalid
        end
      end

      context "プロジェクトがない場合" do
        let(:invalid_suggest) { build(:suggest, project: nil) }

        it "suggestは無効である" do
          expect(invalid_suggest).to be_invalid
        end
      end

      context "画像が6枚を超える場合" do
        let(:suggest) { build(:suggest) }
        let(:image) {
          Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/sample-salad.jpg')), 'image/png')
        }

        it "suggestは無効である" do
          6.times { suggest.photos.build(image: image) }
          expect(suggest).to be_invalid
        end
      end
    end
  end
end
