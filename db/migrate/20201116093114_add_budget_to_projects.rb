class AddBudgetToProjects < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :budget, :decimal, precision: 10
  end
end
