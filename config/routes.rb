Rails.application.routes.draw do
  root 'landing#home'
  get 'signup', to: 'users#new'
  post 'signup', to: 'users#create'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  get 'logout', to: 'sessions#destroy'

  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :sessions, only: [:new, :create, :destory]
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :password_changes, only: [:new, :create, :edit, :update]
  resources :projects, only: [:index, :new, :create, :show, :destroy] do
    resources :suggests, only: [:new, :create, :show, :edit, :update, :destroy]
  end
  resources :comments, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :accept_suggests, only: [:create, :show]
  resources :reviews, only: [:index, :new, :create, :show, :edit, :update]
  resources :email_changes, only: [:edit, :update]
end
