class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.update_attributes(activated: true, activated_at: Time.zone.now)
      log_in user
      submit_cookies(user)
      flash[:success] = "アカウントの認証が完了いたしました。"
      redirect_to user
    else
      flash[:danger] = "無効なリンクです。もう一度はじめからやり直してください。"
      redirect_to root_url
    end
  end
end
