class ProjectsController < ApplicationController
  include Shared::SuggestSupport

  before_action :logged_in_user, only: [:new, :create, :destroy]
  before_action :builder_not_create, only: [:new, :create]

  def index
    @projects = Project.all.includes(:user).paginate(page: params[:page], per_page: 9)
  end

  def new
    @project = current_user.projects.build
    @project.photos.build
  end

  def show
    @project = Project.find_by(id: params[:id])
    if @project.nil?
      flash[:warning] = "お探しのページは見つかりませんでした。"
      redirect_to root_url
    else
      @suggest = already_suggest(@project)
    end
  end

  def create
    @project = Project.new(project_params)
    if @project.save
      flash[:success] = "プロジェクトが投稿されました。"
      redirect_to project_path(@project)
    else
      render 'new'
    end
  end

  def destroy
    project = current_user.projects.find_by(id: params[:id])
    if project.blank?
      flash[:danger] = "該当するプロジェクトが見つかりません。"
      redirect_to root_url
    else
      project.destroy
      flash[:success] = "プロジェクトが削除されました。"
      redirect_to current_user
    end
  end

  private

  def project_params
    params.require(:project).permit(:name,
                                    :content,
                                    :budget,
                                    photos_attributes: [:image]).merge(user_id: current_user.id)
  end

  def builder_not_create
    if current_user.builder?
      redirect_to current_user
      flash[:warning] = "ビルダーはプロジェクトを作成できません。"
    end
  end
end
