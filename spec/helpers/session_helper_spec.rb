require 'rails_helper'

RSpec.describe SessionHelper, type: :helper do
  describe "current_user" do
    let(:user) { create(:user) }

    context "sessionがある場合" do
      before do
        session[:user_id] = user.id
      end

      it "一致するユーザーを返す" do
        expect(current_user).to eq user
      end
    end

    context "cookiesがある場合" do
      before do
        user.update_attribute(:remember_digest, User.digest("remember_token"))
        cookies.permanent.signed[:user_id] = user.id
        cookies.permanent[:remember_token] = "remember_token"
      end

      it "一致するユーザーを返す" do
        expect(current_user).to eq user
      end
    end

    context "ログインしていない場合" do
      it "nilを返す" do
        expect(current_user).to be_nil
      end
    end
  end

  describe "logged_in?" do
    let!(:user) { create(:user) }

    context "sessionがある場合" do
      before do
        session[:user_id] = user.id
      end

      it "trueを返す" do
        expect(logged_in?).to be_truthy
      end
    end

    context "sessionが切れている場合" do
      it "falseを返す" do
        expect(logged_in?).to be_falsey
      end
    end
  end
end
