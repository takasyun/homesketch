class CommentsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: :destroy

  def create
    @comment = Comment.new(comment_params)
    if @comment.save
      redirect_to request.referrer || root_url
    else
      flash[:warning] = "コメントの投稿に失敗しました。"
      redirect_to request.referrer || root_url
    end
  end

  def destroy
    @comment.destroy
    flash[:success] = "コメントが削除されました。"
    redirect_to request.referrer || root_url
  end

  private

  def comment_params
    params.require(:comment).permit(:content,
                                    :suggest_id).merge(user_id: current_user.id)
  end

  def correct_user
    @comment = current_user.comments.find_by(id: params[:id])
    if @comment.nil?
      flash[:warning] = "指定されたコメントは見つかりませんでした。"
      redirect_to request.referrer || root_url
    end
  end
end
