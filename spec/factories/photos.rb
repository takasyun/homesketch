FactoryBot.define do
  factory :suggest_photo, class: "Photo" do
    association :photable, factory: :suggest
    image { Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/sample-salad.jpg')), 'image/png') }
  end

  factory :project_photo, class: "Photo" do
    association :photable, factory: :project
    image { Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/sample-salad.jpg')), 'image/png') }
  end
end
