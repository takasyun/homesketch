class Photo < ApplicationRecord
  belongs_to :photable, polymorphic: true, inverse_of: :photos

  validates :image, presence: true
  validates :photable, presence: true

  mount_uploader :image, ImageUploader
end
