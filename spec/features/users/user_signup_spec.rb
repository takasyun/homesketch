require 'rails_helper'

RSpec.feature "UserSignups", type: :feature do
  feature "ユーザー登録テスト" do
    before do
      ActionMailer::Base.deliveries.clear
      visit signup_path
    end

    context "有効なパラメータをフォームに入力した場合" do
      it "ユーザー登録に成功して認証メールが送られる" do
        expect do
          choose 'user_builder_true'
          fill_in :user_name, with: "example"
          fill_in :user_email, with: "test@test.com"
          fill_in :user_password, with: "password"
          fill_in :user_password_confirmation, with: "password"
          click_on "Sign up"
        end.to change(User, :count).by(1)
        expect(ActionMailer::Base.deliveries.size).to eq 1
      end
    end

    context "無効なパラメータをフォームに入力した場合" do
      it "ユーザー登録に失敗してエラーが出る" do
        expect do
          choose 'user_builder_true'
          fill_in :user_name, with: nil
          fill_in :user_email, with: "test@test.com"
          fill_in :user_password, with: "password"
          fill_in :user_password_confirmation, with: "password"
          click_on "Sign up"
        end.to change(User, :count).by(0)
        expect(ActionMailer::Base.deliveries.size).to eq 0
        expect(page).to have_selector '.alert-danger', text: "エラーが発生しました。"
      end
    end
  end
end
