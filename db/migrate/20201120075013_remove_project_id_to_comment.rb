class RemoveProjectIdToComment < ActiveRecord::Migration[6.0]
  def change
    remove_reference :comments, :project
  end
end
