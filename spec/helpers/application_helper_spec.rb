require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "タイトル表示テスト" do
    it "page_titleがemptyのtitle" do
      expect(helper.full_title('')).to eq('Home-Sketch')
    end

    it "page_titleがnilのtitle" do
      expect(helper.full_title(nil)).to eq('Home-Sketch')
    end

    it "page_titleありのtitle" do
      expect(helper.full_title('test')).to eq('test - Home-Sketch')
    end
  end
end
