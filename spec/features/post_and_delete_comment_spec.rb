require 'rails_helper'

RSpec.feature "PostAndDeleteComments", type: :feature do
  feature "コメント投稿テスト" do
    given(:user) { create(:user, :has_project) }
    given(:suggest) { create(:suggest, project: user.projects.first) }

    before do
      login user
    end

    it "コメントを投稿して、削除する" do
      visit project_suggest_path(suggest.project, suggest)
      fill_in :comment_content, with: "TestComment"
      click_on "コメントを投稿する"
      expect(page).to have_content "TestComment"
      click_on "コメントの削除"
      expect(page).to have_selector '.alert-success', text: "コメントが削除されました。"
    end
  end
end
