require 'rails_helper'

RSpec.feature "PasswordResets", type: :feature do
  feature "パスワードリセットメール送信テスト" do
    given!(:user) { create(:user) }

    before do
      visit new_password_reset_path
      ActionMailer::Base.deliveries.clear
    end

    context "メールアドレスが存在している場合" do
      it "パスワード送信メールが送られる" do
        within(".form") do
          fill_in :password_reset_email, with: user.email
          click_on "ログインリンクを送信"
        end
        expect(page).to have_content "パスワードをリセットするためのリンクが送信されました。"
        expect(ActionMailer::Base.deliveries.size).to eq 1
      end
    end

    context "メールアドレスが存在していない場合" do
      it "パスワード送信メールは送られない" do
        within(".form") do
          fill_in :password_reset_email, with: "invalid"
          click_on "ログインリンクを送信"
        end
        expect(page).to have_content "メールアドレスが正しくありません。"
        expect(ActionMailer::Base.deliveries.size).to eq 0
      end
    end
  end

  feature "パスワードを変更する" do
    given!(:user) { create(:user) }
    given!(:new_pass) { "new_pass" }

    before do
      user.create_reset_digest
      visit edit_password_reset_path(user.reset_token, email: user.email)
    end

    it "パスワードを変更した後ログアウトして再度ログインできる" do
      within(".form") do
        fill_in :user_password, with: new_pass
        fill_in :user_password_confirmation, with: new_pass
        click_on "パスワードをリセット"
      end
      within(".dropdown-menu") { click_on "ログアウト" }
      visit login_path
      within(".form") do
        fill_in :session_email, with: user.email
        fill_in :session_password, with: new_pass
        click_on "ログイン"
      end
      expect(page).to have_content "#{user.name}"
    end
  end
end
