class UserMailer < ApplicationMailer
  def account_activation(user)
    @user = user
    mail to: user.email, subject: "HomeSketch アカウント認証メール"
  end

  def password_reset(user)
    @user = user
    mail to: @user.email, subject: "HomeSketch パスワードリセット"
  end

  def password_change(user)
    @user = user
    mail to: @user.email, subject: "HomeSketch パスワード変更"
  end
end
