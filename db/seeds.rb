User.create!(
  name: "Example User",
  email: "example@test.com",
  password: "foobar",
  password_confirmation: "foobar",
  intro: "これはプロフィールです。はじめまして！",
  website: "https://test@test.com",
  avatar: File.open(Rails.root.join('db/fixtures/avatar/sample-salad.jpg')),
  admin: true,
  activated: true,
  activated_at: Time.zone.now,
  builder: false
)

10.times do |t|
  name  = Faker::Name.name
  email = "example-#{t}@test.com"
  password = "password"
  User.create!(name: name,
               email: email,
               avatar: File.open(Rails.root.join("db/fixtures/avatar/test-avatar#{t}.jpg")),
               password: password,
               password_confirmation: password,
               intro: "これはプロフィールです。はじめまして！",
               activated: true,
               activated_at: Time.zone.now)
end

User.create!(
  name: "モンキーホーム",
  email: "example@builder.com",
  password: "foobar",
  password_confirmation: "foobar",
  intro: "会社概要です。はじめまして！",
  website: "https://builder.test.com",
  avatar: File.open(Rails.root.join('db/fixtures/avatar/test-builder-logo.jpg')),
  activated: true,
  activated_at: Time.zone.now,
  builder: true
)

User.create!(
  name: "アライグマ工務店",
  email: "example@builder2.com",
  password: "foobar",
  password_confirmation: "foobar",
  intro: "会社概要2です。はじめまして！",
  website: "https://builder2.test.com",
  avatar: File.open(Rails.root.join('db/fixtures/avatar/test-builder-logo2.jpg')),
  activated: true,
  activated_at: Time.zone.now,
  builder: true
)

users = User.order(:created_at).take(8)
3.times do |t|
  content = "・土地付きの1軒家で庭付き
  ・キッチンは広めでアイランドキッチン
  ・将来のため子供部屋が2つ欲しい
  ・駐車場はなくても良い
  ・...etc"
  users.each_with_index { |user, index| user.projects.create!(name: "#{t}-#{user.name}邸", content: content, budget: 40000000) }
end

images = []
images << File.open(Rails.root.join("db/fixtures/model-house/kitchen-layout.jpg"))
3.times { |t| images << File.open(Rails.root.join("db/fixtures/model-house/model-house#{t}.jpg")) }
3.times { |t| images << File.open(Rails.root.join("db/fixtures/model-house/drawing#{t}.jpg")) }

users = User.order(:created_at).take(4)
builders = User.order(:created_at).reverse_order.take(2)
builders.each do |builder|
  users.each do |user|
    2.times do |t|
      content = "初めまして！#{builder.name}の営業担当#{Faker::Name.last_name}と申します。
      #{user.name}様のご要望を拝見させていただきましたところ、弊社における以下の設計プランが#{user.name}様にご満足いただけるものだと考えております。
      ・ △△風モダンハウス・プランA
      駐車場はなくても良いとのことですので、間取りが...etc"
      builder.suggests.create!(title: "#{user.name}様への提案", content: content, estimated_amount: 38000000, project_id: user.projects[t].id)
      builder.suggests.last.photos.create!(image: images[rand(1..3)])
      builder.suggests.last.photos.create!(image: images[rand(4..6)])
      builder.suggests.last.photos.create!(image: images[0]) if [true, false].sample
      user.favorite_suggest(Suggest.last) if t == 0
    end
  end
end

users.each do |user|
  user.projects.first.suggests.each do |suggest|
    user.reviews.create!(customer_id: user.id,
                          builder_id: suggest.user.id,
                          suggest_id: suggest.id,
                          content: "良かった",
                          evaluation: rand(2..5))
  end
end

users = User.all
user  = users.first
following = users[1..12]
followers = users[5..12]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
