class EmailChangesController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.authenticate(params[:user][:password])
      if @user.update(email: params[:user][:email])
        redirect_to edit_user_path(@user)
      else
        flash.now[:warning] = "Eメールアドレスの更新に失敗しました。"
        render 'edit'
      end
    else
      flash.now[:danger] = "パスワードが違います。"
      render 'edit'
    end
  end

  private

  def correct_user
    @user = User.find(params[:id])
    unless current_user?(@user)
      flash[:danger] = "編集しているユーザーとログインしているアカウントが一致しません。"
      redirect_to root_url
    end
  end
end
