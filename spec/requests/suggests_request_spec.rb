require 'rails_helper'

RSpec.describe "Suggests", type: :request do
  shared_examples 'error_and_redirect' do
    it "ログインしていないと警告と共にログイン画面にリダイレクトされる" do
      expect(flash[:danger]).to eq "先にログインを行ってください。"
      expect(response).to redirect_to login_url
    end
  end

  describe "GET new_suggest_path" do
    let(:user) { create(:user) }
    let(:builder) { create(:user, :builder) }
    let(:project) { create(:project, user: user) }

    context "ユーザーでログインしている場合" do
      before do
        log_in_as user
        get new_project_suggest_path(project.id)
      end

      it "提案するページにアクセスできない" do
        expect(response).to redirect_to user
        expect(flash[:warning]).to eq "提案を作成できるのはビルダーだけです。"
      end
    end

    context "ビルダーでログインしている場合" do
      context "まだ提案していない場合" do
        before do
          log_in_as builder
          get new_project_suggest_path(project.id)
        end

        it "提案するページにアクセスできる" do
          expect(response).to have_http_status(200)
        end
      end

      context "すでに提案している場合" do
        let!(:suggest) { create(:suggest, user: builder, project: project) }

        before do
          log_in_as builder
          get new_project_suggest_path(project.id)
        end

        it "提案ページにはアクセスできず、リダイレクトされる" do
          expect(response).to redirect_to project
        end
      end
    end

    context "ログインしていない場合" do
      before do
        get new_project_suggest_path(project.id)
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "GET suggest_path" do
    let(:user) { create(:user) }
    let(:project) { create(:project) }
    let(:suggest) { create(:suggest, user: user, project: project) }

    context "ログインしている場合" do
      before do
        log_in_as user
      end

      context "提案されたページが存在する場合" do
        it "提案されたページにアクセスできる" do
          get project_suggest_path(project, suggest)
          expect(response).to have_http_status(200)
        end
      end

      context "提案されたページが存在しない場合" do
        it "rootにリダイレクトされる" do
          get project_suggest_path(project, 999)
          expect(response).to redirect_to root_url
        end
      end
    end

    context "ログインしていない場合" do
      before do
        get project_suggest_path(project, suggest)
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "POST suggests_path" do
    let!(:user) { create(:user) }
    let!(:builder) { create(:user, :builder) }
    let!(:project) { create(:project) }
    let(:valid_suggest_params) { attributes_for(:suggest, project_id: project.id) }
    let(:invalid_suggest_params) { attributes_for(:suggest, project_id: project.id, title: nil) }

    6.times do |i|
      let!(:"image#{i}") {
        Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/sample-salad.jpg')), 'image/png')
      }
    end

    context "ビルダーでログインしている場合" do
      before do
        log_in_as builder
      end

      context "パラメータが適当な場合" do
        it "suggestが投稿できる" do
          expect do
            post project_suggests_path(project), params: { suggest: valid_suggest_params }
          end.to change(Suggest, :count).by(1)
        end
      end

      context "パラメータが不適当な場合" do
        it "エラーが表示される" do
          post project_suggests_path(project), params: { suggest: invalid_suggest_params }
          expect(flash[:warning]).to eq "提案の投稿に失敗しました。"
        end

        it "suggestが投稿できない" do
          expect do
            post project_suggests_path(project), params: { suggest: invalid_suggest_params }
          end.to change(Suggest, :count).by(0)
        end
      end

      context "画像枚数が6枚以上の場合" do
        it "suggestが投稿でない" do
          expect do
            post project_suggests_path(project), params: { suggest: {
              title: "test",
              content: "test",
              estimated_amount: 100,
              project_id: project.id,
              photos_attributes: {
                "0": { image: image0 },
                "1": { image: image1 },
                "2": { image: image2 },
                "3": { image: image3 },
                "4": { image: image4 },
                "5": { image: image5 },
              },
            } }
          end.to change(Suggest, :count).by(0).and change(Photo, :count).by(0)
        end
      end

      context "2回提案を投稿しようとした場合" do
        it "2回目は投稿できない" do
          post project_suggests_path(project), params: { suggest: valid_suggest_params }
          expect do
            post project_suggests_path(project), params: { suggest: valid_suggest_params }
          end.to change(Suggest, :count).by(0)
        end
      end
    end

    context "ユーザーでログインしている場合" do
      before do
        log_in_as user
      end

      it "suggestが投稿できない" do
        expect do
          post project_suggests_path(project), params: { suggest: valid_suggest_params }
        end.to change(Suggest, :count).by(0)
      end

      it "マイページにリダイレクトされる" do
        post project_suggests_path(project), params: { suggest: valid_suggest_params }
        expect(response).to redirect_to user
        expect(flash[:warning]).to eq "提案を作成できるのはビルダーだけです。"
      end
    end

    context "ログインしていない場合" do
      before do
        post project_suggests_path(project), params: { suggest: valid_suggest_params }
      end

      it_behaves_like 'error_and_redirect'
    end
  end

  describe "delete suggest_path" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }
    let!(:project) { create(:project) }
    let!(:suggest) { create(:suggest, user: user) }

    context "ログインしている場合" do
      before do
        log_in_as user
      end

      it "提案が削除される" do
        expect { delete project_suggest_path(project, suggest) }.to change(Suggest, :count).by(-1)
      end
    end

    context "別ユーザーでログインしている場合" do
      before do
        log_in_as other_user
      end

      it "提案は削除されない" do
        expect { delete project_suggest_path(project, suggest) }.to change(Suggest, :count).by(0)
      end
    end

    context "ログインしていない場合" do
      it "ログイン画面にリダイレクトされる" do
        delete project_suggest_path(project, suggest)
        expect(response).to redirect_to login_url
      end

      it "提案は削除されない" do
        expect { delete project_suggest_path(project, suggest) }.to change(Suggest, :count).by(0)
      end
    end
  end
end
