module SessionHelper
  def logged_in?
    current_user.present?
  end

  # rubocop:disable all
  def current_user
    return @current_user if @current_user.present?
    if session[:user_id].present?
      @current_user ||= User.find_by(id: session[:user_id])
    elsif cookies.signed[:user_id].present?
      user = User.find_by(id: cookies.signed[:user_id])
      if user && user.authenticated?(:remember, cookies[:remember_token])
        session[:user_id] = user.id
        @current_user = user
      end
    end
  end
  # rubocop:enable all

  def current_user?(user)
    user && user == current_user
  end
end
