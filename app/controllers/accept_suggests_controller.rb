class AcceptSuggestsController < ApplicationController
  before_action :logged_in_user, only: [:show, :create]
  before_action :correct_user, only: [:show, :create]
  before_action :user_accept_suggest?, only: [:show]

  def show
    @suggest = Suggest.find_by(id: params[:id])
  end

  def create
    current_user.favorite_suggest(@suggest)
    redirect_to accept_suggest_url @suggest
  end

  private

  def correct_user
    @suggest = (Suggest.find_by(id: params[:suggest_id]) or Suggest.find_by(id: params[:id]))
    project_owner = @suggest.project.user
    if project_owner.nil? || !current_user?(project_owner)
      flash[:warning] = "プロジェクトの発注者しか提案の受け入れはできません。"
      redirect_to root_url
    end
  end

  def user_accept_suggest?
    unless current_user.favorite_suggest?(@suggest)
      flash[:warning] = "提案を受け入れていないユーザーが提案受諾画面にアクセスすることはできません。"
      redirect_to project_suggest_url(@suggest.project, @suggest)
    end
  end
end
