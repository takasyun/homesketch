class ReviewsController < ApplicationController
  before_action :logged_in_user, only: [:index, :new, :create, :show, :edit, :update]
  before_action :get_suggest_and_builder, only: [:new, :create, :edit]
  before_action :correct_user, only: [:new, :create, :edit]
  before_action :customer_cant_review_twise, only: [:new, :create]
  before_action :user_accept_suggest?, only: [:new, :create, :edit]

  def index
    @builder = User.find_by(id: params[:id])
    redirect_to root_url and return if @builder.blank?
    @reviews = Review.where(builder_id: params[:id]).paginate(page: params[:page], per_page: 9)
  end

  def new
    @review = Review.new
  end

  def create
    @review = Review.new(create_review_params)
    if @review.save
      flash[:success] = "レビューを投稿しました。"
      redirect_to current_user
    else
      flash.now[:danger] = "レビューを投稿できませんでした。"
      render 'new'
    end
  end

  def show
    @review = Review.find(params[:id])
  end

  def edit
    @review = Review.find(params[:id])
  end

  def update
    @review = Review.find(params[:id])
    if @review.update(update_review_params)
      flash[:success] = "レビューが更新されました。"
      redirect_to review_path(@review)
    else
      render 'edit'
    end
  end

  private

  def create_review_params
    params.require(:review).permit(:suggest_id,
                                   :content,
                                   :evaluation).merge(customer_id: current_user.id, builder_id: @suggest.user.id)
  end

  def update_review_params
    params.require(:review).permit(:content, :evaluation)
  end

  def get_suggest_and_builder
    @suggest ||= Suggest.find_by(id: params[:review][:suggest_id])
    if @suggest.blank?
      flash[:warning] = "レビューする提案が見つかりませんでした。"
      redirect_to current_user
    else
      @builder = @suggest.user
    end
  end

  def correct_user
    unless current_user == @suggest.project.user
      flash[:warning] = "プロジェクト発注者のみがレビューを投稿できます。"
      redirect_to @suggest.project
    end
  end

  def customer_cant_review_twise
    if current_user.already_review?(@suggest)
      flash[:warning] = "すでにレビューを投稿しています。"
      redirect_to current_user
    end
  end

  def user_accept_suggest?
    unless @suggest.project.user.favorite_suggest?(@suggest)
      flash[:warning] = "レビューは提案を受け入れたユーザーしかできません。"
      redirect_to @suggest.project
    end
  end
end
