require 'rails_helper'

RSpec.describe User, type: :model do
  describe "Userバリデーションテスト" do
    context "名前,メールアドレス,パスワードがある場合" do
      let(:valid_user) { build(:user) }

      it "userは有効である" do
        expect(valid_user).to be_valid
      end
    end

    context "無効なパラメータがある場合" do
      context "nameが空欄の場合" do
        let(:invalid_user) { build(:user, name: nil) }

        it "userは無効である" do
          expect(invalid_user).to be_invalid
        end
      end

      context "nameが長すぎる場合" do
        let(:invalid_user) { build(:user, name: 'Alice' * 50) }

        it "userは無効である" do
          expect(invalid_user).to be_invalid
        end
      end

      context "emailが空欄の場合" do
        let(:invalid_user) { build(:user, email: nil) }

        it "userは無効である" do
          expect(invalid_user).to be_invalid
        end
      end

      context "無効なemailの場合" do
        let(:invalid_user) { build(:user, email: 'invalid@test@test') }

        it "userは無効である" do
          expect(invalid_user).to be_invalid
        end
      end

      context "一意のメールアドレスではない場合" do
        let!(:valid_user) { create(:user) }
        let(:invalid_user) { build(:user, email: valid_user.email) }

        it "userは無効である" do
          expect(invalid_user).to be_invalid
        end
      end

      context "passwordがnilの場合" do
        let(:invalid_user) { build(:user, password: nil) }

        it "userは無効である" do
          expect(invalid_user).to be_invalid
        end
      end

      context "passwordの文字数が少ない場合" do
        let(:invalid_user) { build(:user, password: 'fooba', password_confirmation: 'fooba') }

        it "userは無効である" do
          expect(invalid_user).to be_invalid
        end
      end
    end
  end

  describe "authenticated?" do
    let!(:user) { create(:user) }
    let(:token) { User.new_token }

    context "remember_digestがある且、remember_tokenが一致する場合" do
      before do
        user.update_attribute(:remember_digest, User.digest(token))
      end

      it "trueを返す" do
        expect(user.authenticated?(:remember, token)).to be_truthy
      end
    end

    context "remember_digestがない場合" do
      it "falseを返す" do
        expect(user.authenticated?(:remember, token)).to be_falsey
      end
    end

    context "remember_tokenが一致しない場合" do
      before do
        user.update_attribute(:remember_digest, User.digest('false'))
      end

      it "falseを返す" do
        expect(user.authenticated?(:remember, token)).to be_falsey
      end
    end
  end
end
