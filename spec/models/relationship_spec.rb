require 'rails_helper'

RSpec.describe Relationship, type: :model do
  describe "relationshipバリデーションテスト" do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user) }

    context "followed_idとfollower_idが存在する場合" do
      let(:valid_relationship) { user.active_relationships.build(followed_id: other_user.id) }

      it "relatinshipは有効である" do
        expect(valid_relationship).to be_valid
      end
    end

    context "followed_idがnilの場合" do
      let(:invalid_relationship) { user.active_relationships.build(followed_id: nil) }

      it "relatinshipは無効である" do
        expect(invalid_relationship).to be_invalid
      end
    end

    context "follower_idがnilの場合" do
      let(:invalid_relationship) { user.passive_relationships.build(follower_id: nil) }

      it "relatinshipは無効である" do
        expect(invalid_relationship).to be_invalid
      end
    end
  end
end
