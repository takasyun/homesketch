class Review < ApplicationRecord
  belongs_to :customer, class_name: 'User'
  belongs_to :suggest
  belongs_to :builder, class_name: 'User'

  validates :customer_id, presence: true
  validates :builder_id, presence: true
  validates :suggest_id, presence: true
  validates :content, presence: true
  validates :evaluation, numericality: { only_integer: true, greater_than: 0, less_than: 6 }, presence: true
end
