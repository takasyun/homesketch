class User < ApplicationRecord
  attr_accessor :remember_token, :activation_token, :reset_token, :change_token

  before_create :create_activation_digest

  has_many :projects, dependent: :destroy
  has_many :active_relationships, class_name: 'Relationship', foreign_key: 'follower_id', dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :passive_relationships, class_name: 'Relationship', foreign_key: 'followed_id', dependent: :destroy
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :comments, dependent: :destroy
  has_many :suggests, dependent: :destroy
  has_many :accept_suggests, foreign_key: 'user_id', dependent: :destroy
  has_many :approve_suggests, through: :accept_suggests, source: :approval_suggest
  has_many :reviews, foreign_key: 'customer_id'
  has_many :evaluations, class_name: 'Review', foreign_key: 'builder_id', dependent: :destroy

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze

  validates :name, presence: true, length: { maximum: 50 }
  validates :email,
            length: { maximum: 100 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false },
            presence: true
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  validates :website, format: { with: /\A#{URI.regexp(%w(http https))}\z/ }, allow_blank: true
  validates :intro, length: { maximum: 150 }

  mount_uploader :avatar, AvatarUploader

  has_secure_password

  class << self
    def digest(string)
      if ActiveModel::SecurePassword.min_cost
        cost = BCrypt::Engine::MIN_COST
      else
        cost = BCrypt::Engine.cost
      end
      BCrypt::Password.create(string, cost: cost)
    end

    def new_token
      SecureRandom.urlsafe_base64
    end
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def create_remember_token
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def delete_remember_token
    update_attribute(:remember_digest, nil)
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def create_change_digest
    self.change_token = User.new_token
    update_attribute(:change_digest, User.digest(change_token))
    update_attribute(:change_sent_at, Time.zone.now)
  end

  def digest_expired?(attribute)
    send("#{attribute}_sent_at") < 2.hours.ago
  end

  def follow(user)
    following << user
  end

  def following?(user)
    following.include?(user)
  end

  def favorite_suggest(suggest)
    approve_suggests << suggest
  end

  def favorite_suggest?(suggest)
    approve_suggests.include?(suggest)
  end

  def already_review?(suggest)
    reviews.include? suggest.review
  end

  private

  def create_activation_digest
    self.activation_token  = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
end
