class CreateEvaluateBuilders < ActiveRecord::Migration[6.0]
  def change
    create_table :evaluate_builders do |t|
      t.integer :customer_id
      t.integer :builder_id
      t.text :content
      t.integer :evaluation

      t.timestamps
    end
  end
end
