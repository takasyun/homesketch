require 'rails_helper'

RSpec.describe Review, type: :model do
  describe "reviewのバリデーションテスト" do
    context "有効なパラメータの場合" do
      let(:valid_review) { build(:review) }

      it "reviewは有効である" do
        expect(valid_review).to be_valid
      end
    end

    context "customer_idがない場合" do
      let(:invalid_review) { build(:review, customer_id: nil) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end

    context "builder_idがない場合" do
      let(:invalid_review) { build(:review, builder_id: nil) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end

    context "suggest_idがない場合" do
      let(:invalid_review) { build(:review, suggest_id: nil) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end

    context "contentがない場合" do
      let(:invalid_review) { build(:review, content: nil) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end

    context "evaluationがない場合" do
      let(:invalid_review) { build(:review, evaluation: nil) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end

    context "evaluationが大きい場合" do
      let(:invalid_review) { build(:review, evaluation: 100) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end

    context "evaluationが小さい場合" do
      let(:invalid_review) { build(:review, evaluation: -10) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end

    context "evaluationが整数でない場合" do
      let(:invalid_review) { build(:review, evaluation: 0.3) }

      it "reviewは無効である" do
        expect(invalid_review).to be_invalid
      end
    end
  end
end
