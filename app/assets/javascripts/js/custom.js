//============================== header =========================
$(function(){
  const window_size = window.matchMedia("(max-width:993px)");

  if(window_size.matches){
    $(".wide-window").each(function(){
      $(this).hide();
    });
    $("#narrow-window").show();
  }else{
    $(".wide-window").each(function(){
      $(this).show();
    });
    $("#narrow-window").hide();
  }
  window_size.addListener(function (){
    if(window_size.matches){
      $(".wide-window").each(function(){
        $(this).hide();
      });
      $("#narrow-window").show();
    }else{
      $(".wide-window").each(function(){
        $(this).show();
      });
      $("#narrow-window").hide();
    }
  });
});
//============================== photo-uploader =========================
$(function(){
  let model_name = null;
  if (window.location.href.match(/\/suggests/)) {
    model_name = "suggest";
    $('.preview-box').length == 5 ? $('#label-field').hide() : null;
  } else if (window.location.href.match(/\/projects/)) {
    model_name = "project";
    $('.preview-box').length == 5 ? $('#label-field').hide() : null;
  }
  // プレビュー挿入html
  $(function(){
    function buildHTML(id) {
      const html = `<div class="preview-box" id="preview-box_${id}">
                      <div class="upper-box">
                        <img src="" alt="preview">
                      </div>
                      <div class="lower-box">
                        <label class="update-box" id="edit_btn_${id}">
                          <span>編集</span>
                        </label>
                        <div class="delete-box" id="delete_btn_${id}">
                          <span>削除</span>
                        </div>
                      </div>
                    </div>`
      return html;
    }
    // 画像のアップロード
    $(document).on('change', '.hidden-field', function() {
      const id = $(this).attr('id').replace(/[^0-9]/g, '');
      const reader = new FileReader();
      reader.readAsDataURL(this.files[0]);
      $(reader).on('load', function() {
        const image = this.result;
        if (!$(`#preview-box_${id}`).length) {
          $('#label-field').before(buildHTML(id));
        }
        $(`#preview-box_${id} img`).attr('src', `${image}`);
        $(`#${model_name}_photos_attributes_${id}__destroy`).prop('checked', false);
        if ($('.preview-box').length == 5) {
          $('#label-field').hide();
        }
        for (let size = 0; size < 5; size++) {
          const photo = $(`#${model_name}_photos_attributes_${size}_image`).val();
          const display_photo = $(`#preview-box_${size} img`);
          if (!photo[0] && !(display_photo.length)) {
            $('.photo-label').attr({id: `photo-label-${size}`, for: `${model_name}_photos_attributes_${size}_image`});
            return false;
          }
        }
      });
    });
    // 画像の削除
    $(document).on('click', '.delete-box', function() {
      const id = $(this).attr('id').replace(/[^0-9]/g, '');
      $(`#preview-box_${id}`).remove();
      $(`#${model_name}_photos_attributes_${id}_image`).val("");
      $(`#${model_name}_photos_attributes_${id}__destroy`).prop('checked', true);
      const size = $('.preview-box').length;
      if (size < 5) {
        $('#label-field').show();
      }
      //画像削除したfile_fieldにfocusする
      $('.photo-label').attr({id: `photo-label-${id}`, for: `${model_name}_photos_attributes_${id}_image`});
    });
    // 画像の編集
    $(document).on('click', '.update-box', function() {
      const id = $(this).attr('id').replace(/[^0-9]/g, '');
      $(`#edit_btn_${id}`).attr({for: `${model_name}_photos_attributes_${id}_image`});
    });
  });
});
